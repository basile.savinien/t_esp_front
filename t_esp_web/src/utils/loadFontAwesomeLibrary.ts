import { library } from '@fortawesome/fontawesome-svg-core'
import { faArrowRight, faBookmark, faEllipsisVertical, faGamepad, faVolumeHigh, faCircleCheck } from '@fortawesome/free-solid-svg-icons'
import {  faBookmark as farBookmark } from '@fortawesome/free-regular-svg-icons'


export const loadFontAwesomeLibrary = () => {
    return library.add(
        faArrowRight,
        faBookmark,
        farBookmark,
        faEllipsisVertical,
        faGamepad,
        faVolumeHigh,
        faCircleCheck
    )
}
