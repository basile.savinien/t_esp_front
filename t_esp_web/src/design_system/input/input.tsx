import React, { useCallback, useRef, useState, HTMLProps} from "react";
import styles from "./input_styles.module.css"
import classNames from "classnames";


interface inputType extends HTMLProps<HTMLInputElement> {
    title?: string
    containerClassName?: string
    className?: string
    titleClassName?: string
    color?: "primary" | "secondary" | "none"
}

const Input = ({title, containerClassName, color = "primary", titleClassName, className,...props} : inputType) => {
    const inputRef = useRef<HTMLInputElement>(null);
    const [isFocus, setIsFocus] = useState(false)

    const focus = useCallback(() => {
        if (inputRef.current) {
            inputRef?.current?.focus()
        }
    },[inputRef.current])


    const onFocus = useCallback(() => {
        setIsFocus(true)
    },[])

    const onBlur = useCallback(() => {
        setIsFocus(false)
    },[])
    return(
        <div className={classNames(styles.container, containerClassName)}>
            {title && <span onClick={focus} className={classNames(styles.title,titleClassName, isFocus && styles[`titleFocus_${color}`])}>{title}</span>}
            <input
                onFocus={onFocus}
                onBlur={onBlur}
                className={classNames(styles.input,className, isFocus && styles[`inputFocus_${color}`])}
                ref={inputRef}
                {...props}
            />
        </div>

    )
}

export default Input
