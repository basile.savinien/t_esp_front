import React, {FC} from "react";
import cn from "classnames"
import styles from './Button.module.css';
import {Button as ButtonMantine, ButtonProps} from '@mantine/core';


const Button : FC<ButtonProps<any>>  =  ({
                                        children,
                                        className,
                                        size='md',
                                        color= 'primary',
                                        ...props
}) => {
    return(
        <ButtonMantine
            {...props}
            className={cn( className, props.uppercase ? styles.buttonMaj : styles.button)}
            size={size}
            color={color}

        >
         {children}
        </ButtonMantine>
    )
}

export default Button;
