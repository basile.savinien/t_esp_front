import React, {useEffect, useRef, useState} from "react";
import {animated, useSpring} from "react-spring";
import styles from "./select_color.module.css";
import cn from "classnames";


interface SelectType {
    options: OptionsType[]
    value: any
    onChange?: (value: any, index: number) => any
    pickerClassName?: string
    enableClickOutside?: boolean
    onEnterOption?: (value: any, index: number) => any
    onLeaveOption?: (value: any, index: number) => any
}

interface OptionsType {
    name: string
    value: string
    selected?: boolean
}

const SelectColor = ({
                         options,
                         value,
                         onChange,
                         pickerClassName,
                         enableClickOutside = true,
                         onEnterOption,
                         onLeaveOption
}: SelectType) => {
    const [isOpen, setIsOpen] = useState<boolean>(false);
    const selectRef = useRef<HTMLDivElement>(null);
    const props = useSpring({
        opacity: isOpen ? 1 : 0 ,
        transform: isOpen ? 'translateY(5px)' : "translateY(10px)",
        config: { duration: 200}
    })

    useEffect(() => {
        if (enableClickOutside) {
            document.addEventListener('click',handleClickOutside)
        }
        return () => {
            document.removeEventListener('click', handleClickOutside);

        }
    },[enableClickOutside, isOpen])

    const handleClickOutside = ( event: MouseEvent) => {
        const {target} = event;
        if(!selectRef.current?.contains(target as Node) && isOpen ) {
            toggleDropdown(false);
        }
    }

    const toggleDropdown = (value : boolean | null = null ) => {
        setIsOpen((prevState => typeof value === 'boolean' ? value : !prevState));
    }

    const onChangeValue = (val: any, i: number) => {
        if (onChange) {
            onChange(val, i);
        }
        toggleDropdown(false);
    }
    const color = value?.includes('#') ? value : options.find((v) => v.name === value)?.value;

    return(
        <div className={styles.select} ref={selectRef}>
            <div className={cn(styles.picker, pickerClassName)} onClick={() => toggleDropdown()} style={{backgroundColor: color}} />
            <animated.div className={cn(styles.dropdown, !isOpen && styles.close)} style={props} >
                {
                    options.map(({value, name}, index) => {
                        return(
                            <div
                                data-value={value}
                                data-index={index}
                                className={styles.optionContainer}
                                onClick={() => onChangeValue({name,value}, index)}
                                onMouseEnter={()=> onEnterOption?.({name,value}, index)}
                                onMouseLeave={()=> onLeaveOption?.({name,value}, index)}
                                key={`${name}_${value}`}
                            >
                                <div className={styles.option}  style={{backgroundColor: value}} />
                            </div>
                        )
                    })
                }
            </animated.div>
        </div>
    )
}

export default SelectColor;
