import React, {HTMLProps} from 'react';
import styles from './radio_styles.module.css';
import cn from "classnames";

interface RadioType extends  HTMLProps<HTMLInputElement> {
    type : 'radio' | 'checkbox',
    label: string
    color?: 'primary' | 'secondary'
}

const Radio = ({type, label, value, name, color = 'primary', ...props}:RadioType) => {
    return (
        <>
            <div className={styles.container}>
                    <input id={value?.toString()} type={type} className={styles.radio} value={value}   {...props} />
                    <span className={cn(styles.check, color && styles[color])}/>
                    <label htmlFor={value?.toString()} className={styles.label}>{label}</label>
            </div>
        </>
    )
}

export default Radio;
