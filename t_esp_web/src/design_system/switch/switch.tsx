import styles from './switch.module.css' ;
import {useSpring, animated, to} from "react-spring";
import cn from 'classnames';
interface SwitchProps {
    value?: boolean
    color?: 'primary' | 'secondary'
    onChange?: (isChecked: boolean) => void
    scale?: number
    label?: string
    labelPosition?: 'before' | 'after'
    containerClassName?: string
    textClassName?: string
}


const dimensions = {
    width: 45,
    height: 25
}


const Switch = ({
                    value = true,
                    color='primary',
                    onChange,
                    scale = 1,
                    label,
                    labelPosition= 'before',
                    containerClassName,
                    textClassName
}:SwitchProps ) => {
    const {x} = useSpring({
        x: value ? dimensions.width - dimensions.height : 0,
    })

    const onChangeAction = () => {
            onChange?.(!value);
    }

    return(
        <div className={cn(styles.containerWithLabel,containerClassName )}>
            {
                labelPosition === 'before' && label && (
                    <span onClick={onChangeAction} className={cn(styles.label, textClassName)}>{label}</span>
                )
            }
            <div className={styles.container}
                 style={{
                    width: dimensions.width,
                    height: dimensions.height,
                    backgroundColor: value ? `var(--${color}-color)`: '#aaa',
                    border: `${value ? `var(--${color}-color)` : '#AAA'} 3px solid`,
                     ...scale && {transform: `scale(${scale})`},
                     margin: `${10 + (dimensions.height * (scale -  1 ) / 2 )}px ${10 + (dimensions.width * (scale -  1 ) / 2 )}px `
                }}
                 onClick={onChangeAction}
            >
                <animated.div  className={styles.round} style={{transform: to([x], (x) => `translateX(${x}px)`)}} />
            </div>
            {
                labelPosition === 'after' && label && (
                    <span onClick={onChangeAction} className={cn(styles.label, textClassName)}>{label}</span>
                )
            }
        </div>
    )
}

export default Switch;
