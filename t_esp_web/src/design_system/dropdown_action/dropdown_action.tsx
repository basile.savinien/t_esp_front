import React, {
    ReactElement,
    useEffect,
    useRef,
    useState
} from "react";
import styles from './dropdown_action_styles.module.css';
import {useSpring, animated} from "react-spring";
import cn from "classnames";
interface ActionType {
    component: ReactElement
    options: OptionsType[]
    enableClickOutside?: boolean
    enableHover?: boolean
}

interface OptionsType {
    text: string
    action: () => void
}

const DropdownAction = ({
                            component,
                            options,
                            enableClickOutside = true,
                            enableHover = false

} : ActionType) => {
    const [isOpen, setIsOpen] = useState<boolean>(false);
    const selectRef = useRef<HTMLDivElement>(null);
    const props = useSpring({
        opacity: isOpen ? 1 : 0 ,
        transform: isOpen ? 'translateY(5px)' : "translateY(10px)",
        config: { duration: 200 }
    })

    useEffect(() => {
        if (enableClickOutside) {
            document.addEventListener('click',handleClickOutside)
        }
        return () => {
            document.removeEventListener('click', handleClickOutside);

        }
    },[enableClickOutside, isOpen])

    const handleClickOutside = ( event: MouseEvent) => {
        const {target} = event;
        if(!selectRef.current?.contains(target as Node) && isOpen ) {
            toggleDropdown(false);
        }
    }

    const toggleDropdown = (value : boolean | null = null ) => {
        setIsOpen((prevState => typeof value === 'boolean' ? value : !prevState));
    }

    const onClickAction = (action: any, event: React.MouseEvent<HTMLElement>) => {
        event.stopPropagation();
        action?.();
        toggleDropdown();

    }

    const componentWithProps = React.cloneElement(component, {
        onClick:() => toggleDropdown(),
        ...enableHover && {
            onMouseEnter :() => enableHover && toggleDropdown(true)
        }
    })

    return(
        <div
            className={styles.select}
            ref={selectRef}
            onMouseLeave={() => enableHover && toggleDropdown(false)}
        >
            {componentWithProps}
            <div className={cn(styles.hitSlop, !isOpen && styles.close)}>
                <animated.div className={styles.dropdown} style={props}>
                    {
                        options.map(({action, text}, index) => {
                            return(
                                <div
                                    data-index={index}
                                    className={styles.optionContainer}
                                    onClick={(event) => onClickAction(action, event)}
                                    key={`${text}`}
                                >
                                    <span className={styles.option}  >{text}</span>
                                </div>
                            )
                        })
                    }
                </animated.div>
            </div>

        </div>
    )
}

export default DropdownAction;
