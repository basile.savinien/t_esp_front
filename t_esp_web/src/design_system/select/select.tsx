import React, {HTMLProps, ReactChild, useCallback, useEffect, useRef, useState} from "react";
import styles from './select_styles.module.css';
import {useSpring, animated} from "react-spring";
import cn from "classnames";
interface SelectType {
    options: OptionsType[]
    value: any
    onChange?: (value: any, index: number) => any
    disabled?: boolean
    inputClassName?: string
    containerSelect?: string
    enableClickOutside?: boolean
    onEnterOption?: (value: any, index: number) => any
    onLeaveOption?: (value: any, index: number) => any
}

interface OptionsType {
    name: string
    value: string | number
    selected?: boolean
}

const Select = ({
                    options,
                    value,
                    onChange,
                    disabled = false,
                    inputClassName,
                    containerSelect,
                    enableClickOutside = true,
                    onEnterOption,
                    onLeaveOption
} : SelectType) => {
    const [isOpen, setIsOpen] = useState<boolean>(false);
    const selectRef = useRef<HTMLDivElement>(null);
    const props = useSpring({
        opacity: isOpen ? 1 : 0 ,
        transform: isOpen ? 'translateY(5px)' : "translateY(10px)",
        config: { duration: 200 }
    })

    useEffect(() => {
        if (enableClickOutside) {
            document.addEventListener('click',handleClickOutside)
        }
        return () => {
            document.removeEventListener('click', handleClickOutside);

        }
    },[enableClickOutside, isOpen])

    const handleClickOutside = ( event: MouseEvent) => {
        const {target} = event;
        if(!selectRef.current?.contains(target as Node) && isOpen ) {
            toggleDropdown(false);
        }
    }

    const toggleDropdown = (value : boolean | null = null ) => {
        setIsOpen((prevState => typeof value === 'boolean' ? value : !prevState));
    }

    const onChangeValue = (val: any, i: number) => {
        if (onChange) {
            onChange(val, i);
        }
        toggleDropdown(false);
    }

    return(
            <div className={cn(styles.select, containerSelect)} ref={selectRef}>
                <div className={cn(styles.input, disabled && styles.disabled, inputClassName)} onClick={() => !disabled &&  toggleDropdown()}>
                    <span>{options.find((option) => option.value === value)?.name}</span>
                    <div className={styles.icon}>{'>'}</div>
                </div>
                <animated.div className={cn(styles.dropdown, !isOpen && styles.close)} style={props} >
                    {
                        options.map(({value, name}, index) => {
                            return(
                                <div
                                    data-value={value}
                                    data-index={index}
                                    className={styles.optionContainer}
                                    onClick={() => onChangeValue(value, index)}
                                    onMouseEnter={()=> onEnterOption?.(value, index)}
                                    onMouseLeave={()=> onLeaveOption?.(value, index)}
                                    key={`${name}_${value}`}
                                >
                                    <span className={styles.option}  >{name}</span>
                                </div>
                            )
                        })
                    }
                </animated.div>
            </div>
    )
}

export default Select;
