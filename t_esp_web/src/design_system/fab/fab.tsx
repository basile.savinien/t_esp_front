import React, {HTMLProps, ReactElement} from 'react';
import styles from './fab_styles.module.css';
import cn from "classnames";

interface FabType extends  HTMLProps<HTMLDivElement> {
    text?: string
    icon?: ReactElement
    classNameContainer?: string
    classNameText?: string
    color?: 'primary' | 'secondary'
    variant?: 'standard' | 'transparent'
}

const Fab = ({text, icon, color = 'primary', variant = 'standard', classNameContainer, classNameText, ...props}:FabType) => {
    return (
        <>
            <div className={cn(styles.container, styles[color], styles[variant], classNameContainer)} {...props}>
                <div className={cn(styles.overlay, styles[color], styles[variant])}/>
                {icon && icon}
                {text && <span className={cn(styles.text, color && styles[color], styles[variant], classNameText)}> {text} </span>}
            </div>
        </>
    )
}

export default Fab;
