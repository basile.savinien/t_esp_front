import styles from './settings_styles.module.css';
import {AvatarType} from "@common/types/utils/avatar";
import {formField, formType} from "@common/types/utils/form";
import React, {useState} from "react";
import {useStore} from "@hooks/redux/useStore";
import {formFieldDefault} from "@common/defaultValue/form";
import Input from "@src/design_system/input/input";
import AvatarPicker from "@components/avatar_picker/avatar_picker";
import {defaultAvatar} from "@common/defaultValue/avatar";
import Button from "@src/design_system/button/Button";
import {formSetting, userType} from "@common/types/users/users";
import {editMe} from "@common/actions/user";
import {useDispatch} from "react-redux";
import Loader from "react-loader-spinner";
import {Redirect} from "react-router-dom";





const Settings = () => {
    const dispatch = useDispatch();
    const user = useStore(store => store.user);
    const [loading, setLoading] = useState<boolean>(false);
    const [form, setForm] = useState<formSetting>({
        username: formFieldDefault(formType.STRING, user.username),
        email: formFieldDefault(formType.STRING, user.email),
        oldPassword: formFieldDefault(formType.STRING),
        password: formFieldDefault(formType.STRING),
        avatar: formFieldDefault(formType.OBJECT, user.avatar || defaultAvatar),
    });

    const onChangeField = <K extends keyof formSetting> (key: K) => (event: React.ChangeEvent<HTMLInputElement>) => {
        const { value}  = event.target;
        setForm({...form,
            [key]: {
                ...form[key],
                value,
            }
        })
    }

    const onChangeAvatar = (avatar: AvatarType) => {
        setForm({...form, avatar: {
                ...form.avatar,
                value: avatar
            }})
    }

    const save = () => {
        setLoading(true);
        const data = Object.entries(form).reduce((acc,[key, field])=> {
            const isValid = field.value && field.value !== user[key as keyof userType];
            return({
                ...acc,
                ...isValid && {[key]: field.value}
            })
        },{});
        editMe(data)(dispatch)
            .finally(() => setLoading(false));
    }

    if(!user.isAuth) {

        return <Redirect to={'/'} />
    }


    return(
        <div className={styles.container}>
            <div className={styles.formContainer}>
                <div className={styles.columnForm}>
                    <Input
                        title={'Username'}
                        value={form.username.value}
                        onChange={onChangeField('username')}
                    />
                    <Input
                        title={'Email'}
                        value={form.email.value}
                        onChange={onChangeField('email')}
                    />
                </div>
                <div className={styles.columnForm}>
                    <Input
                        title={'Ancien Mot de passe'}
                        value={form.oldPassword.value}
                        onChange={onChangeField('oldPassword')}
                    />
                    <Input
                        title={'Mot de passe'}
                        value={form.password.value}
                        onChange={onChangeField('password')}
                    />
                </div>

            </div>
            <AvatarPicker
                avatar={form.avatar.value}
                className={styles.avatar}
                onEdit={onChangeAvatar}
            />

            <div className={styles.buttonContainer}>
            {!loading ? (
                    <Button
                        color={'secondary'}
                        className={styles.button}
                        onClick={save}
                        uppercase
                        size={'lg'}
                    >
                        Sauvegarder
                    </Button>
                ) :
                    <Loader
                        type="Oval"
                        color={"var(--secondary-color)"}
                        height={50}
                        width={50}
                    />
            }
            </div>
        </div>
    )
}

export default Settings;
