import React, {useEffect, useState} from "react";
import styles from './musics_styles.module.css';
import {getMusics} from "@common/actions/musics";
import Header from '@components/musics/header/header'
import Card from "@components/musics/card/card";
import {useAppDispatch, useStore} from "@hooks/redux/useStore";
import {UPDATE_LIST_MUSIC} from "@common/storeType";
import {Music} from "@common/types/musics/musics";

const Musics = () => {
    const dispatch = useAppDispatch();
    const musics = useStore( store => store.listMusic.data);
    const filters = useStore( store => store.listMusic?.filters)

    useEffect(() => {
        getMusics(filters)(dispatch, UPDATE_LIST_MUSIC);
    },[filters.identifier])

    return(
        <div className={styles.container} >
            <Header />
            <div className={styles.musicList}>
                {musics.map((music: Music, index: number) => {
                    return(
                        <Card music={music} key={`${music.title}_${index}`} />
                    )
                })}
            </div>

        </div>
    )
}

export default Musics;
