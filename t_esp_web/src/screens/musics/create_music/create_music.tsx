import {formField, formType} from "@common/types/utils/form";
import {MusicType} from "@common/types/musics";
import {createMusicForm} from "@common/types/store/reducer/createMusicReducer";

import React, {useState} from "react";
import Button from "@src/design_system/button/Button";
import styles from './create_music.module.css';
import Input from "@src/design_system/input/input";
import cn from 'classnames';
import TagsContainer from "@components/create_music/tags/tags_container";
import {useAppDispatch, useStore} from "@hooks/redux/useStore";
import {createMusic, resetField, updateField} from "@common/actions/createMusic";
import {formFieldDefault} from "@common/defaultValue/form";
import Loader from "react-loader-spinner";
import Radio from "@src/design_system/radio/radio";


const CreateMusic = () => {
    const dispatch = useAppDispatch();
    const {form, error} = useStore(store => store.createMusic);
    const [keyword, setKeyword] = useState<formField<string>>(formFieldDefault(formType.STRING));
    const [loading, setLoading] = useState(false);


    const onChangeType = (event : React.ChangeEvent<HTMLInputElement>) => {
        const {value}  = event.target;
        if(Object.values(MusicType).includes(value as MusicType) ) {
            updateField('type',value as MusicType)(dispatch);
            if (value !== MusicType.MUSIC) {
                resetField('authors')(dispatch)
            }
        }
    }

    const onChange = (name: keyof createMusicForm) => (event: React.ChangeEvent<HTMLInputElement>) => {
        const { value}  = event.target;
        updateField(name,value)(dispatch);
    }

    const onChangeKeyword = (event: React.ChangeEvent<HTMLInputElement>) => {
        const {value} = event.target;
        setKeyword((prevState) => ({...prevState,value}));
    }

    const addKeyword =() => {
        if (!keyword.value) return
        const newKeywords = [...form.keywords, keyword.value];
        updateField("keywords", newKeywords )(dispatch)
        setKeyword(formFieldDefault(formType.STRING))
    }

    const onKeyPress = (event: React.KeyboardEvent) => {
         if(event.key === 'Enter') {
             addKeyword?.();
         }
    }

    const updateAuthor = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { value}  = event.target;
        updateField('authors',value.split(';'))(dispatch);
    }

    const addFile = (event: React.ChangeEvent<HTMLInputElement>) => {
        const file = event.currentTarget.files?.item(0);
        if (file) {
            updateField("file", file)(dispatch);
        }
    }


    const createMusicCallback = () => {
        setLoading(true)
        createMusic(form)(dispatch).finally(()=> setLoading(false))
    }

    return(
        <div className={styles.container} >
            <h1 className={styles.title}>Créer une musique</h1>
            <div className={cn(styles.row, styles.radioRow)} onChange={onChangeType}>
                <Radio type={'radio'} label={'musique'} checked={form.type === MusicType.MUSIC} value={MusicType.MUSIC} name={'type'} />
                <Radio type={'radio'} label={'film'} checked={form.type === MusicType.MOVIE}  value={MusicType.MOVIE} name={'type'}/>
                <Radio type={'radio'} label={'serie'} checked={form.type === MusicType.SERIE} value={MusicType.SERIE} name={'type'}/>
                <Radio type={'radio'} label={'anime'} checked={form.type === MusicType.ANIME} value={MusicType.ANIME} name={'type'}/>
                <Radio type={'radio'} label={'cartoon'} checked={form.type === MusicType.CARTOON} value={MusicType.CARTOON} name={'type'}/>
            </div>
            <div className={styles.row}>
                <Input
                    title={"Titre"}
                    name={"title"}
                    placeholder={"Titre de la musique"}
                    containerClassName={styles.input}
                    className={styles.input}
                    value={form.title}
                    onChange={onChange('title')}
                    disabled={loading}
                />
                {form.type === 'music' && <Input
                    title={"Auteurs"}
                    name={"authors"}
                    placeholder={"Auteurs de la musique ( ; séparateur)"}
                    containerClassName={styles.input}
                    value={form.authors?.join(';')}
                    onChange={updateAuthor}
                    disabled={loading}
                />}
            </div>
            <div className={cn(styles.row, styles.alignItemEnd)}>
                <Input
                    title={"Mots clés"}
                    name={"keywords"}
                    placeholder={"Mots qui déclenchent une bonne réponse"}
                    containerClassName={styles.input}
                    value={keyword.value}
                    onChange={onChangeKeyword}
                    onKeyPress={onKeyPress}
                    disabled={loading}
                />
                <Button
                    variant={'light'}
                    className={styles.button}
                    onClick={addKeyword}
                    disabled={loading}
                >
                    Ajouter
                </Button>
            </div>
            <div className={styles.row}>
                <TagsContainer tags={form.keywords} />
            </div>
            <div className={styles.rowTitleContainer}>
                <span className={styles.rowTitle}>Fichier</span>
            </div>
            <div className={styles.row}>
                <label htmlFor={"music-input"} className={styles.input_label}>
                    <Button variant={'outline'} uppercase size={'xl'}>
                        {form.file?.name || "choisir une musique"}
                    </Button>
                </label>
                <input type="file"
                       id="music-input"
                       className={styles.input_file}
                       name="music"
                       accept=".mp3,audio/*"
                       onChange={addFile}
                       disabled={loading}
                />

            </div>
            {!loading ? (
                <div>
                    <Button
                        uppercase
                        size={'lg'}
                        className={styles.create_button}
                        onClick={createMusicCallback}
                        disabled={loading}
                    >
                        Créer
                    </Button>
                </div>
                ) :
                <div style={{marginTop: 50}}>
                    <Loader
                        type="Oval"
                        color={"#245FAD"}
                        height={50}
                        width={50}
                    />
                </div>
            }

        </div>
    )
}

export default CreateMusic;
