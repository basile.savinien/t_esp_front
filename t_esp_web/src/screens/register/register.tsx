import React, {useCallback, useState} from "react";
import styles from "./register_styles.module.css";
import Input from "@src/design_system/input/input";
import Button from "@src/design_system/button/Button";
import {useDispatch} from "react-redux";
import {register} from "@common/actions/auth";
import {Redirect} from "react-router-dom";
import {useStore} from "@hooks/redux/useStore";
import {Link} from 'react-router-dom';
import AvatarPicker from "@components/avatar_picker/avatar_picker";
import {AvatarType} from "@common/types/utils/avatar";
import {defaultAvatar} from "@common/defaultValue/avatar";

interface formType  {
    email: string
    username: string
    password: string
    avatar:AvatarType
}


const Register = () => {
    const dispatch = useDispatch();
    const {isAuth} = useStore((store) => store.user);
    const [form, setForm] = useState<formType>({
        email: "",
        username: "",
        password: "",
        avatar: defaultAvatar
    })

    const onChange = useCallback((event) => {
        const {name, value} = event.target
        setForm(prevState => ({...prevState,[name]: value}));
    },[])

    const validate = () => {
        register(form)(dispatch);
    };

    const onEditAvatar = (avatar: AvatarType) => {
        setForm(prevState => ({...prevState,avatar}));
    }

    if (isAuth) {
        return <Redirect to={'/'} />;
    }

    return(
        <div className={styles.container} >
            <h1 className={styles.title}>S'ENREGISTRER</h1>
            <div className={styles.form}>
                <Input
                    title={"Email"}
                    name={"email"}
                    value={form.email}
                    onChange={onChange}
                    placeholder={"Exemple: pierre.dupond@gmail.com"}
                    containerClassName={styles.inputContainer}
                    className={styles.input}
                    titleClassName={styles.label}
                />
                <Input
                    title={"Username"}
                    name={"username"}
                    value={form.username}
                    onChange={onChange}
                    placeholder={"Exemple: Pierro"}
                    containerClassName={styles.inputContainer}
                    className={styles.input}
                    titleClassName={styles.label}
                />
                <Input
                    title={"Mot de passe"}
                    name={"password"}
                    value={form.password}
                    onChange={onChange}
                    placeholder={"Mot de passe secret"}
                    containerClassName={styles.inputContainer}
                    className={styles.input}
                    type={"password"}
                    titleClassName={styles.label}
                />
                <AvatarPicker avatar={form.avatar} onEdit={onEditAvatar}  />

                <Button
                    className={styles.button}
                    onClick={validate}
                    uppercase
                    size={'lg'}
                >
                    Valider
                </Button>
                <span className={styles.link}>vous possédez déjà un compte ? <Link to={'/login'}> Se connecter</Link></span>
            </div>
        </div>
    )
}

export default Register;
