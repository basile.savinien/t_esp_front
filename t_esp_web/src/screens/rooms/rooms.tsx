import {Link, useHistory} from "react-router-dom";
import Button from "@src/design_system/button/Button";
import styles from "./rooms.module.css";
import React, {useState} from "react";
import {useStore} from "@hooks/redux/useStore";
import {createRoom} from "@common/actions/rooms";
import {useDispatch} from "react-redux";
import Switch from "@src/design_system/switch/switch";

const Rooms = ( ) => {
    const dispatch = useDispatch();
    const user = useStore(store => store.user);
    const history = useHistory();

    const [isPublic, setIsPublic] = useState<boolean>(true)

    const onClickCreate = () => {
        createRoom(isPublic)(dispatch).then((room: any)=> {
            history.push(`rooms/${room?._id}`)
        })
    }

    return(
        <div className={styles.container}>
            {(user.isAuth) &&
            (
                <div className={styles.createContainer}>
                    <Switch
                        value={isPublic}
                        onChange={(isChecked) => setIsPublic(isChecked)}
                        color={'secondary'}
                        label={'public'}
                        containerClassName={styles.switch}
                        textClassName={styles.label}

                    />
                    <Button
                        className={styles.button_create}
                        onClick={onClickCreate}
                        color={'primary'}
                        uppercase
                    >
                        create
                    </Button>
                </div>

            )

            }
        </div>
    );
}

export default Rooms;
