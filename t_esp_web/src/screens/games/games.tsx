import {useStore} from "@hooks/redux/useStore";
import CreateAnonymousUser from "@components/create_anonymous_user/create_anonymous_user";
import {UserRole} from "@common/types/users/users";
import Lobby from "@components/lobby/lobby";


const Games = () => {
    const user = useStore(store => store.user);

    if (user.role === UserRole.GUEST && !user._id) {
        return <CreateAnonymousUser />;
    }

    return <Lobby />;
};

export default Games;
