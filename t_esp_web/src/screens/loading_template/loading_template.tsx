import React from 'react';
import styles from './loading_template.module.css';
import LogoLoader from "@assets/logo_loader/logo_loader";
import LoadingBar from "@components/loading_bar/loading_bar";

const LoadingTemplate = () => {
    return(
        <div className={styles.container} >
            <LoadingBar />
            <LogoLoader height={300} width={300} fill={'#fff'} />
        </div>
    );
}

export  default LoadingTemplate;
