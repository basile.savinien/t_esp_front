import {formLogin} from "@common/types/auth/index";
import React, {useCallback, useState} from "react";
import styles from "./login_styles.module.css";
import Input from "@src/design_system/input/input";
import Button from "@src/design_system/button/Button";
import {login} from "@common/actions/auth";
import {useDispatch} from "react-redux";
import {useStore} from "@hooks/redux/useStore";
import {Link, Redirect} from "react-router-dom";




const Login = () => {
    const dispatch = useDispatch();
    const {isAuth} = useStore((store) => store.user);
    const [form, setForm] = useState<formLogin>({
        email: "",
        password: ""
    });

    const onChange = useCallback((event) => {
        const {name, value} = event.target
        setForm((prevState) => ({...prevState,[name]: value}))
    },[])

    const validate = useCallback(() => {
        login(form)(dispatch);
    },[form.email, form.password]);

    if (isAuth) {
        return <Redirect to={'/'} />;
    }

    return(
        <div className={styles.container} >
            <h1 className={styles.title}>SE CONNECTER</h1>
            <div className={styles.form}>
                <Input
                    title={"Email"}
                    name={"email"}
                    value={form.email}
                    onChange={onChange}
                    placeholder={"Exemple: pierre.dupond@gmail.com"}
                    containerClassName={styles.input}
                />
                <Input
                    title={"Mot de passe"}
                    name={"password"}
                    value={form.password}
                    onChange={onChange}
                    placeholder={"Mot de passe secret"}
                    containerClassName={styles.input}
                    type={"password"}
                />

                <Button
                    className={styles.button}
                    onClick={validate}
                    uppercase
                    size={'lg'}
                >
                    Valider
                </Button>
                <span className={styles.link}>vous n'avez pas encore de compte? <Link to={'/register'}> S'enregister</Link></span>
            </div>
        </div>
    )
}

export default Login;
