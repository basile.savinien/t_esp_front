import styles from './edit_playlist.module.css';
import UpdatePlailistUI from "@components/playlists/update_playlist/update_playlist";
import {useAppDispatch, useStore} from "@hooks/redux/useStore";
import React, {useEffect, useState} from "react";
import {editPlaylist, initializeEdit} from "@common/actions/editPlaylist";
import {useParams} from "react-router-dom";
import Loader from "react-loader-spinner";
import {editPlaylistForm} from "@common/types/store/reducer/editPlaylistReducer";

interface LocationState {
    id: string
}

const EditPlaylist = () => {
    const {id}  = useParams<LocationState>();
    const dispatch = useAppDispatch();
    const id_store = useStore(store => store.editPlaylist.id);
    const [loading, setLoading] = useState<boolean>(false);

    useEffect(() => {
        if(id !== id_store ) {
            setLoading(true);
            initializeEdit(id)(dispatch).then(() => {
                setLoading(false);
            })
        }
    },[]);

    const validate = (form: editPlaylistForm) => {
        editPlaylist(id,form)(dispatch);
    }

    if (loading) {
        return(
            <div className={styles.loaderContainer}>
                <Loader
                    type="Oval"
                    color={"#245FAD"}
                    height={60}
                    width={60}
                />
            </div>
        );
    }

    return(
        <UpdatePlailistUI validate={validate} validateText={'save'}  />
    )
}

export default EditPlaylist;
