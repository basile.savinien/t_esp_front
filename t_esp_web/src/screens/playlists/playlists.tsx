import React, {useEffect, useState} from "react";
import styles from './playlists_styles.module.css';
import Header from '@components/playlists/header/header';
import Tabs from "@components/playlists/tabs/tabs";
import {Link} from "react-router-dom";
import Button from "@src/design_system/button/Button";
import {useAppDispatch, useStore} from "@hooks/redux/useStore";
import {getPlaylists} from "@common/actions/playlists";
import Card from "@components/playlists/card/card";
import ModalPlaylist from "@screens/playlists/modal/modal";
import {Playlist} from "@common/types/playlists/playlists";

const Playlists = () => {
    const dispatch = useAppDispatch();
    const user = useStore(store => store.user)
    const filters = useStore(store => store.listPlaylist.filters);
    const tab = useStore(store => store.listPlaylist.tab);
    const playlists = useStore(store => store.listPlaylist);

    const [selectedPlaylist, setSelectedPlaylist] = useState<Playlist | null>(null)

    const tabContent = playlists?.[tab];


    useEffect(() => {
        getPlaylists(filters, tab)(dispatch)
    },[tab, filters.identifier])

    const selectPlaylist = (event: React.MouseEvent<HTMLDivElement> ,playlist: Playlist) => {
        setSelectedPlaylist(playlist);
    }

    return(
        <div style={{flex:1, display: 'flex'}}>
            <div className={styles.container} >
                <Tabs />
                <Header />
                {(user.isAuth) && <Link to={'/Playlist/Create'}>
                    <Button
                        className={styles.button_create}
                        uppercase
                    >
                        create
                    </Button>
                </Link>
                }
                <div className={styles.content}>
                    {
                        tabContent.data.map((playlist) => {
                            return <Card
                                action={(event: React.MouseEvent<HTMLDivElement>) => selectPlaylist(event, playlist)}
                                key={`${tab}_${playlist._id}`}
                                playlist={playlist}
                                selected={selectedPlaylist?._id === playlist._id}
                            />;
                        })
                    }
                </div>
            </div>
            <ModalPlaylist playlist={selectedPlaylist} onClose={() => setSelectedPlaylist(null)} />
        </div>
    )
}

export default Playlists;
