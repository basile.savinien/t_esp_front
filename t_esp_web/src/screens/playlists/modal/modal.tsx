import React, {useEffect, useState} from "react";
import styles from './modal_styles.module.css';
import {useSpring, animated} from "react-spring";
import { useGesture } from '@use-gesture/react'
import useLocalStorage from "@src/hooks/useLocalStorage";
import Fab from "@src/design_system/fab/fab";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Card from "@components/musics/card/card";
import {SaveButton} from "@components/playlists/save_button/save_button";
import {Playlist} from "@common/types/playlists/playlists";

interface ModalType {
    playlist: Playlist | null
    onClose: () => void
}

const CLOSE_VALUE = 0;

const ModalPlaylist = ({playlist, onClose} : ModalType) => {
    const [defaultWidth, setDefaultWidth] = useLocalStorage<number>("modal_playlist_width",500);
    const [isOpen, setIsOpen] = useState<boolean>(false);

    useEffect(() => {
        if(playlist?._id && !isOpen ) {
            setIsOpen(true);
        }
        if(playlist === null && isOpen ) {
            setIsOpen(false);
        }
    },[playlist?._id]);

    useEffect(()=> {
        if (isOpen) {
            api.start({x: defaultWidth});
        } else {
            api.start({x: CLOSE_VALUE});
        }
    },[isOpen])

    const [{ x }, api] = useSpring(() => ({
        x:isOpen ? defaultWidth : CLOSE_VALUE,
    }))
    const {opacity} = useSpring({opacity : isOpen ? 1 : 0})

    const bind = useGesture(
        {
            onDrag: ({movement: [x]}) => api.start({
        x:  defaultWidth - x ,
    }),
            onDragEnd: ({movement: [x]}) => setDefaultWidth((prevState: number) => prevState - x),
        }
    )

    const close = () => {
      setIsOpen(false);
      onClose?.();
    }

    return(
        <animated.div  className={styles.container} style={{width: x, opacity}}>
            <div {...bind()} className={styles.divider}>
                <div className={styles.resize} >
                    <div className={styles.resizeOverlay} >
                        <div className={styles.resizeIcon}>
                            <div className={styles.row}>
                                <div className={styles.dot} />
                                <div className={styles.dot} />
                            </div>
                            <div className={styles.row}>
                                <div className={styles.dot} />
                            </div>
                            <div className={styles.row}>
                                <div className={styles.dot} />
                                <div className={styles.dot} />
                            </div>
                            <div className={styles.row}>
                                <div className={styles.dot} />
                            </div>
                            <div className={styles.row}>
                                <div className={styles.dot} />
                                <div className={styles.dot} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className={styles.content}>
                <div className={styles.header}>
                    <Fab
                        icon={<FontAwesomeIcon icon={"arrow-right"} size={'2x'} color={"var(--secondary-color)"} />}
                        onClick={close}
                        variant={"transparent"}
                        classNameContainer={styles.icon}
                    />
                    <h2 className={styles.title}>{playlist?.name}</h2>
                    {playlist && <SaveButton playlist={playlist} className={styles.save_button}/>}
                </div>
                <div className={styles.list}>
                    {
                        playlist?.musics.map((_id, index) => {
                            return <Card key={`${_id}_${index}`} music={_id} />
                        })
                    }
                </div>

            </div>
        </animated.div>
    )
}

export default ModalPlaylist;
