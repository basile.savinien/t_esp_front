import { jsx as _jsx } from "react/jsx-runtime";
import { useEffect } from "react";
import UpdatePlailistUI from "@components/playlists/update_playlist/update_playlist";
import { useAppDispatch, useStore } from "@hooks/redux/useStore";
import { initializeEdit } from "@common/actions/editPlaylist";
import { createPlaylist } from "@common/actions/createPlaylist";
var CreatePlaylist = function () {
    var dispatch = useAppDispatch();
    var id = useStore(function (store) { return store.editPlaylist.id; });
    useEffect(function () {
        if (id !== null) {
            initializeEdit()(dispatch);
        }
    }, []);
    var validate = function (form) {
        createPlaylist(form)(dispatch);
    };
    return (_jsx(UpdatePlailistUI, { validate: validate, validateText: 'create' }, void 0));
};
export default CreatePlaylist;
