import React, {useEffect} from "react";
import UpdatePlailistUI from "@components/playlists/update_playlist/update_playlist";
import {useAppDispatch, useStore} from "@hooks/redux/useStore";
import {initializeEdit} from "@common/actions/editPlaylist";
import {createPlaylist} from "@common/actions/createPlaylist";
import {editPlaylistForm} from "@common/types/store/reducer/editPlaylistReducer";


const CreatePlaylist = ( ) => {
    const dispatch = useAppDispatch();
    const id = useStore(store => store.editPlaylist.id);
    useEffect(() => {
        if(id !== null ) {
            initializeEdit()(dispatch);
        }
    },[]);

    const validate = (form: editPlaylistForm) => {
        createPlaylist(form)(dispatch)
    }

    return(
        <UpdatePlailistUI
            validate={validate}
            validateText={'create'}
        />
    );
}

export default CreatePlaylist;
