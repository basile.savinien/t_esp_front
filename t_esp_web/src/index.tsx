import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import './styles/site.scss';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {Provider} from 'react-redux';
import {store} from "@common/reducers";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import 'rc-slider/assets/index.css';
import {loadFontAwesomeLibrary} from "@src/utils/loadFontAwesomeLibrary";
import 'antd/dist/antd.css';

import { MantineProvider} from '@mantine/core';

loadFontAwesomeLibrary();

ReactDOM.render(
  <React.StrictMode>
      <Provider store={store}>
          <MantineProvider
              theme={{
                  fontFamily: 'var(--primary-font)',
                  colors: {
                      'primary': ['#e6eef9', '#cddef4', '#9bbdea', '#699cdf', '#377bd5', '#245fad', '#245FAD', '#20559b', '#1c4c8a', '#194279'],
                      'secondary': ['#fce8ef','#fad2df', '#f6a4c0', '#f178a1', '#ed4b82', '#eb3472', '#e91e63', '#d71557', '#bf124d', '#a71043'],
                  },
              }}
          >
            <App />
        </MantineProvider>
      </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
