import React, {useEffect} from 'react';
import styles from './App.module.css';
import NavBar from "@components/nav_bar/nav_bar";
import SideBar from "@components/side_bar/side_bar";

import {HEIGHT_NAV_BAR} from "@src/config";

import {
    BrowserRouter as Router,
    Route
} from "react-router-dom";
import Login from "@screens/login/login"
import Register from "@screens/register/register";
import Musics from "@screens/musics/musics";
import {getMe} from "@common/actions/user";
import {useDispatch} from "react-redux";
import {useStore} from "@hooks/redux/useStore";
import LoadingTemplate from "@screens/loading_template/loading_template";
import useDelay from "@hooks/time/useDelay";
import CreateMusic from "@screens/musics/create_music/create_music";
import Playlists from "@screens/playlists/playlists";
import CreatePlaylist from "@screens/playlists/create_playlist/create_playlist";
import EditPlaylist from "@screens/playlists/edit_playlist/edit_playlist";
import Rooms from "@screens/rooms/rooms";

import Games from "@screens/games/games";
import Settings from "@screens/settings/settings";


function App() {
    const dispatch = useDispatch();
    const user = useStore(store => store.user);
    const accessToken = user.accessToken || localStorage.getItem('accessToken');
    const done = useDelay(1000, !!accessToken);

    useEffect(()=> {
        getMe()(dispatch);

    },[]);

    if ((accessToken && !user.hasFetch) || !done) {
        return <LoadingTemplate />;
    }

  return (
      <Router>
        <div className={styles.app} >
            <NavBar />
            <div className={styles.content} style={{height: `calc(100vh - ${HEIGHT_NAV_BAR}px)`}}>
                <SideBar />
                <Route path={'/Login'} exact component={Login} />
                <Route path={'/Register'} exact component={Register} />
                <Route path={'/Musics'} exact component={Musics} />
                <Route path={'/Music/Create'} exact component={CreateMusic} />
                <Route path={'/Playlists'} exact component={Playlists} />
                <Route path={'/Playlist/Create'} exact component={CreatePlaylist} />
                <Route path={'/Playlist/Edit/:id'} exact component={EditPlaylist} />
                <Route path={'/Rooms'} exact component={Rooms} />
                <Route path={'/Rooms/:id'} exact component={Games} />
                <Route path={'/Settings'} exact component={Settings} />
            </div>
        </div>
      </Router>
  );
}

export default App;
