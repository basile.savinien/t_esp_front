// @ts-ignore
import bufferConcat from  'array-buffer-concat'
import { useEffect, useRef} from "react";
import {useStore} from "@hooks/redux/useStore";
import {cleanBuffer} from "@common/actions/rooms";
import {useDispatch} from "react-redux";
import {RESET_PLAYER} from "@common/storeType";




export const usePlayerMusic  = () : void => {
    const dispatch = useDispatch();
    const {audio, volume} = useStore(store => store.player);
    const {buffer} = useStore(store => store.room);

    const loadMusic = async () => {
        if (buffer) {
            volume.gain.value = parseFloat(localStorage.getItem('volume') || "0.5");
            volume.connect(audio.destination);
            const arrayBuffer = bufferConcat(...buffer);
            let source = audio.createBufferSource();
            const newBuffer =  await audio.decodeAudioData(arrayBuffer);
            source.buffer =  newBuffer;
            source.connect(volume);
            source.start(0);
            source.onended = (event) => {
                cleanBuffer()(dispatch);
            }
        }
    }
    useEffect( () => {
        loadMusic();
    },[buffer]);


    useEffect(() => {
        return () => {
            if (audio.state !== "closed") {
                audio?.close();
                cleanBuffer()(dispatch);
                dispatch({
                    type: RESET_PLAYER
                });
            }
        }
    },[]);

}
