import {Music} from "@common/types/musics/musics";
import styles from './card_styles.module.css';
import cn from "classnames";
import useMusic from "@hooks/musics/useMusic";

interface CardType {
    music: Music | Music["_id"]
    action?: () => any
    containerClassName?: string
}



const Card = ({music, action, containerClassName}: CardType) => {
    const data = useMusic(music);

    if (!data) {
        return null
    }

    return(
        <div className={cn(styles.container, containerClassName)} onClick={action}>
            <span className={styles.title}>{data.title}</span>
            <div className={styles.backRow}>
                {
                    !!data.artists?.length ? (
                        <span className={styles.authors}>({data.artists?.join(', ')})</span>
                    ) :(
                        <div/>
                    )
                }
                <span className={styles.type}>{data.type} </span>
            </div>
        </div>
    );

}

export default Card;
