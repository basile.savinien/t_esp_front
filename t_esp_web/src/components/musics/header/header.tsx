import React from 'react';
import {Link} from "react-router-dom";
import Button from "@src/design_system/button/Button";
import styles from './header_styles.module.css'
import { useStore} from "@hooks/redux/useStore";
import {updateFilters} from "@common/actions/musics";
import Search from "@components/musics/search/search";
import {useRights} from "@hooks/users/useRights";
import {UserRole} from "@common/types/users/index";

const Header = () => {
    const filters = useStore( store => store?.listMusic?.filters)
    const hasRight = useRights([UserRole.ADMIN, UserRole.MODERATOR])

    return(
        <div className={styles.container}>
            <Search updateSearch={updateFilters} filters={filters} />

            {hasRight && <Link to={'/Music/Create'}>
                <Button
                    className={styles.button_create}
                    uppercase
                >
                    create
                </Button>
            </Link>}
        </div>
    );
}

export default Header;
