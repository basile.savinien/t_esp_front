import {FiltersMusic} from "@common/types/musics/musics";
import styles from "./search_styles.module.css";
import Select from "@src/design_system/select/select";
import Input from "@src/design_system/input/input";
import cn from "classnames";
import React from "react";
import {useAppDispatch, useStore} from "@hooks/redux/useStore";


interface SearchProps {
    updateSearch: (filters: {[key in keyof FiltersMusic ]?:  FiltersMusic[key]}) => any
    filters: FiltersMusic
}

export const Search = ({updateSearch, filters}: SearchProps) => {
    const dispatch = useAppDispatch();

    const onChangeSelect = (value: FiltersMusic['type']) => {
        updateSearch({type: value})(dispatch)
    }

    const onChange = (name: keyof FiltersMusic) => (event: React.ChangeEvent<HTMLInputElement>) => {
        const { value}  = event.target;
        updateSearch({[name]: value})(dispatch);
    }

    const onChangeArray = (name: keyof FiltersMusic) => (event: React.ChangeEvent<HTMLInputElement>) => {
        const { value}  = event.target;
        updateSearch({[name]: (!!value && value?.split(';')) || []})(dispatch);
    }
    return(
        <div className={styles.filters}>
            <Select
                options={[
                    {name: 'Tous', value: 'all'},
                    {name: 'Musique', value: 'music'},
                    {name: 'Film', value: 'movie'},
                    {name: 'Série', value: 'serie'},
                    {name: 'Anime', value: 'anime'},
                    {name: 'Dessin anime', value: 'cartoon'},
                ]}
                value={filters.type}
                onChange={onChangeSelect}
                inputClassName={styles.selectInput}
            />
            <Input
                name={"title"}
                placeholder={"Rechercher par titre"}
                containerClassName={styles.inputFilters}
                className={styles.input}
                value={filters.title}
                onChange={onChange('title')}
            />
            <Input
                name={"artists"}
                placeholder={"Rechercher par artiste (séparateur ; )"}
                containerClassName={cn(styles.inputFilters, styles.inputFiltersArtists)}
                className={styles.input}
                value={filters.artists.join(';')}
                onChange={onChangeArray('artists')}
            />
        </div>
    )
}

export default Search;
