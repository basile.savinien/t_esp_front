import Tag from "@src/design_system/tag/tag";
import styles from './tags_container_styles.module.css';
import {useCallback} from "react";
import {useAppDispatch} from "@hooks/redux/useStore";
import {updateField} from "@common/actions/createMusic";

interface TagsType {
    tags: string[]
}

const TagsContainer = ({tags} : TagsType) => {
    const dispatch = useAppDispatch();

    const deleteTag = useCallback((event, index) => {
        const newTags = tags.filter((value, i) => {
            return i !== index;
        });
        updateField("keywords", newTags)(dispatch);
    },[JSON.stringify(tags)])

    return(
        <div className={styles.container}>
            {
                tags.map((tag,index) => {
                    return(
                        <Tag name={tag} index={index} deleteCallback={deleteTag} />
                    )
                })
            }
        </div>
    );
}

export  default TagsContainer;
