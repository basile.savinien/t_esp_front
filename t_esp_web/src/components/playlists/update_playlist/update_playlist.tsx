import styles from "./update_playlist.module.css";
import React, {useEffect, useState} from "react";
import Search from "@components/musics/search/search";
import {getMusics} from "@common/actions/musics";
import { UPDATE_SEARCH_EDIT_PLAYLIST} from "@common/storeType";
import {useAppDispatch, useStore} from "@hooks/redux/useStore";
import Card from "@components/musics/card/card";
import Input from "@src/design_system/input/input";
import Button from "@src/design_system/button/Button";
import Loader from "react-loader-spinner";
import Radio from "@src/design_system/radio/radio";
import {editPlaylistForm} from "@common/types/store/reducer/editPlaylistReducer";
import {
    addMusicToEditPlaylist,
    removeMusicToEditPlaylist,
    updateFieldEditPlaylist,
    updateFiltersEditPlaylist
} from "@common/actions/editPlaylist";


interface UpdatePlaylistType {
    validate: (form: editPlaylistForm) => void
    validateText: string
}

const UpdatePlailistUI = ({validate, validateText}: UpdatePlaylistType ) => {
    const dispatch = useAppDispatch();
    const filters = useStore(store => store.editPlaylist.search.filters);
    const musics = useStore(store => store.editPlaylist.search.data);
    const form = useStore(store => store.editPlaylist.form);
    const selectedMusics = useStore(store => store.editPlaylist.form.musics)

    const [loading, setLoading] = useState(false);

    useEffect(() => {
        getMusics(filters)(dispatch, UPDATE_SEARCH_EDIT_PLAYLIST);
    },[filters.identifier])

    const onChange = (name: keyof editPlaylistForm) => (event: React.ChangeEvent<HTMLInputElement>) => {
        const { value}  = event.target;
        updateFieldEditPlaylist(name,value)(dispatch);
    }

    const onChangeCheckbox = (name: keyof editPlaylistForm) => (event: React.ChangeEvent<HTMLInputElement>) => {
        const { checked}  = event.target;
        updateFieldEditPlaylist(name,checked)(dispatch);
    }

    const submitForm = async () => {
        setLoading(true);
        await validate(form)
        setLoading(false)
    }

    return(
        <div className={styles.container} >
            <div className={styles.content}>
                <div className={styles.column}>
                    <h2 className={styles.columnTitle}>Search</h2>
                    <Search updateSearch={updateFiltersEditPlaylist} filters={filters} />
                    <div className={styles.listMusics}>
                        {musics.filter((music) => !selectedMusics.find((val) => val._id === music._id)).map((music, index) => {
                            return(
                                <Card
                                    containerClassName={styles.card}
                                    music={music} key={`${music.title}_${index}`}
                                    action={() => addMusicToEditPlaylist(music)(dispatch)}
                                />
                            )
                        })}
                    </div>
                </div>
                <div className={styles.separator} />
                <div className={styles.column}>
                    <h2 className={styles.columnTitle}>Ma playlist</h2>
                    <Input
                        name={"Nom"}
                        placeholder={"Nom de la playlist"}
                        value={form.name}
                        onChange={onChange('name')}
                        containerClassName={styles.inputName}
                    />
                    <Radio
                        value={'private'}
                        type={'checkbox'}
                        label={"privée"}
                        color={'secondary'}
                        checked={form.private}
                        onChange={onChangeCheckbox('private')}
                    />
                    <div className={styles.listMusics}>
                        {selectedMusics.map((music, index) => {
                            return(
                                <Card
                                    containerClassName={styles.card}
                                    music={music}
                                    key={`playlist_${music.title}_${index}`}
                                    action={() => removeMusicToEditPlaylist(music)(dispatch)}
                                />
                            )
                        })}
                    </div>
                </div>
            </div>
            {!loading ? (
                <div>
                    <Button
                        uppercase
                        className={styles.create_button}
                        onClick={submitForm}
                        disabled={loading}
                        size={'lg'}
                    >{validateText}</Button>
                </div>
                ) :
                <div style={{marginTop: 50}}>
                    <Loader
                        type="Oval"
                        color={"#245FAD"}
                        height={50}
                        width={50}
                    />
                </div>
            }
        </div>
    );
}

export default UpdatePlailistUI;
