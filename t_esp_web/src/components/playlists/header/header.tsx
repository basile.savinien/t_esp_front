import React from 'react';
import styles from './header_styles.module.css'
import Search from "@components/playlists/search/search";
import {updateFiltersPlaylists} from "@common/actions/playlists";
import {useStore} from "@hooks/redux/useStore";
import {initialSearch} from "@common/defaultValue/playlist";

const Header = () => {
    const {filters, tab} = useStore(store => store.listPlaylist);
    const tabFilters = tab === filters.tab ? filters : {...initialSearch, tab};
    return(
        <div className={styles.container}>
            <Search updateSearch={updateFiltersPlaylists} filters={tabFilters} />
        </div>
    );
}

export default Header;
