import {FiltersPlaylist} from "@common/types/playlists/playlists";
import styles from "./search_styles.module.css";
import Select from "@src/design_system/select/select";
import Input from "@src/design_system/input/input";
import React from "react";
import {useAppDispatch} from "@hooks/redux/useStore";
import  { Range } from 'rc-slider';

interface SearchProps {
    updateSearch: (filters: {[key in keyof FiltersPlaylist ]?:  FiltersPlaylist[key]}) => any
    filters: FiltersPlaylist
}

export const Search = ({updateSearch, filters}: SearchProps) => {
    const dispatch = useAppDispatch();

    const onChangeSelect = (value: FiltersPlaylist['type']) => {
        updateSearch({type: value})(dispatch)
    }

    const onChange = (name: keyof FiltersPlaylist) => (event: React.ChangeEvent<HTMLInputElement>) => {
        const { value}  = event.target;
        updateSearch({
            ...filters,
            [name]: value}
        )(dispatch);
    }

    const onChangeRange = (value: number[]) => {
        updateSearch({
            ...filters,
            range: {
                min: value[0],
                max: value[1]
            }
        })(dispatch)
    }
    return(
        <div className={styles.filters}>
            <Select
                options={[
                    {name: 'Tous', value: 'all'},
                    {name: 'Mixte', value: 'mixte'},
                    {name: 'Musique', value: 'music'},
                    {name: 'Film', value: 'movie'},
                    {name: 'Série', value: 'serie'},
                    {name: 'Anime', value: 'anime'},
                    {name: 'Dessin anime', value: 'cartoon'},
                ]}
                value={filters.type}
                onChange={onChangeSelect}
                inputClassName={styles.selectInput}
            />
            <Input
                name={"name"}
                placeholder={"Rechercher par nom"}
                containerClassName={styles.inputFilters}
                className={styles.input}
                value={filters.name}
                onChange={onChange('name')}
            />
            <div className={styles.sliderContainer}>
                <span>{`nombre de musiques: ${filters.range.min} - ${filters.range.max}`}</span>
                <Range
                    min={0}
                    max={30}
                    defaultValue={[0,30]}
                    value={[filters?.range?.min || 0, filters?.range?.max|| 30]}
                    trackStyle={[{backgroundColor: 'var(--secondary-color)'}]}
                    handleStyle={[{
                        borderColor: 'var(--secondary-color)',
                        boxShadow: 'none'
                    }, {
                        borderColor: 'var(--secondary-color)',
                        boxShadow: 'none'
                    }]}
                    onChange={onChangeRange}
                />
            </div>
        </div>
    )
}

export default Search;
