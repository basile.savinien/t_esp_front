import styles from './delete_playlist_styles.module.css';
import {Playlist} from "@common/types/playlists";
import {Modal} from "@mui/material";
import {useSpring, animated} from "react-spring";
import Button from "@src/design_system/button/Button";
import {useDispatch} from "react-redux";
import {deletePlaylist} from "@common/actions/playlists";


type ModalWithoutChildren = Omit<React.ComponentProps<typeof Modal>, "children">

interface ModalProps extends ModalWithoutChildren {
    playlist: Playlist
}



const DeletePlaylistModal = ({playlist, open, onClose = () => {}, ...props} : ModalProps) => {
    const dispatch = useDispatch();
    const style = useSpring({
        from: { opacity: 0 },
        to: { opacity: open ? 1 : 0 },
    });

    const handleClose = (event: React.MouseEvent<HTMLElement>) => {
        event.stopPropagation();
        onClose(event, 'backdropClick')
    }

    const handleDelete = (event: React.MouseEvent<HTMLElement>) => {
        event.stopPropagation();
        deletePlaylist(playlist._id)(dispatch)
    }

    return(
        <Modal
            open={open}
            onClose={handleClose}
            {...props}
        >
            <animated.div style={style} className={styles.container}>
                <h2 className={styles.title}>Supprimer la Playlist {playlist.name}</h2>
                <p className={styles.text}>Êtes-vous sur de vouloir supprimer cette playlist ? <br/> Pour continuer, cliquez sur Supprimer </p>
                <div className={styles.action}>
                    <Button onClick={handleClose} className={styles.button} variant={'light'} >Annuler</Button>
                    <Button onClick={handleDelete} className={styles.button} color={'secondary'}>Supprimer</Button>
                </div>
            </animated.div>
        </Modal>

        );
}

export default DeletePlaylistModal;
