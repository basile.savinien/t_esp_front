import {Playlist} from "@common/types/playlists/playlists";
import Fab from "@src/design_system/fab/fab";
import styles from "./save_button_styles.module.css";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {Tooltip} from "antd";
import {savePlaylist} from "@common/actions/playlists";
import {useStore} from "@hooks/redux/useStore";
import {useDispatch} from "react-redux";
import {HTMLProps} from "react";
import useIsSaved from "@hooks/playlists/useIsSaved";

interface PropsButton extends HTMLProps<HTMLDivElement>{
    playlist: Playlist

}

export const SaveButton = ({playlist, ...props}: PropsButton) => {
    const {isAuth} = useStore(store => store.user);
    const dispatch = useDispatch();
    const isSaved = useIsSaved(playlist._id);
    const savePlaylistAction = (event: React.MouseEvent<HTMLElement>) => {
        event.stopPropagation();
        savePlaylist(playlist._id)(dispatch)
    }

    if(!isAuth) return null;

    return(
        <div {...props}>
            <Tooltip title="Sauvegarder" placement="topRight">
                <div onClick={savePlaylistAction}>
                    <Fab
                        variant={"transparent"}
                        classNameContainer={styles.save}
                        icon={
                            <FontAwesomeIcon
                                icon={[isSaved ? "fas" : "far","bookmark"]}
                                size={"lg"}
                                color={!isSaved ? '#aaa': 'var(--secondary-color)'}
                            />
                        }
                    />

                </div>
            </Tooltip>
        </div>
    )
}
