import {Playlist} from "@common/types/playlists/playlists";
import styles from './card_styles.module.css';
import cn from "classnames";
import {SaveButton} from "@components/playlists/save_button/save_button";
import {ActionPlaylist} from "@components/playlists/action_playlist/action_playlist";
import usePlaylist from "@hooks/playlists/usePlaylist";

interface CardType {
    playlist: Playlist | Playlist["_id"]
    action?: (event: React.MouseEvent<HTMLDivElement>) => any
    containerClassName?: string
    selected?: boolean
    enableDropdown?: boolean
    enableSave?: boolean
}



const Card = ({
                playlist,
                action,
                containerClassName,
                selected = false,
                enableDropdown = true,
                enableSave = true
}: CardType) => {
    const data = usePlaylist(playlist);
    if (!data) {
        return null
    }

    return(
            <div className={cn(styles.container, selected && styles.selected , containerClassName)} onClick={action}>
                <div className={styles.firstRow}>
                    <span className={styles.title}>{data.name}</span>
                    <div className={styles.actions}>
                        { enableSave && <SaveButton playlist={data}/>}
                        { enableDropdown && <ActionPlaylist playlist={data}/>}
                    </div>
                </div>
                <div className={styles.backRow}>
                    <span className={styles.type}>{data.type} </span>
                    <span className={styles.length}>{data.length} musics</span>
                </div>
            </div>


    );

}

export default Card;
