import styles from './tabs_styles.module.css';
import cn from "classnames";
import React from "react";
import {animated, useSpring} from "react-spring";
import {useAppDispatch, useStore} from "@hooks/redux/useStore";
import {switchPlaylistTab} from "@common/actions/playlists";
import {PlaylistTab} from "@common/types/playlists/playlists";

interface tabProps {
    name: string
    value: string
    isSelected: boolean
    select: (name: string) => void
}

const Tabs = () => {
    const dispatch = useAppDispatch();
    const {tab} = useStore(store => store.listPlaylist)
    const user = useStore(store => store.user)

    return(
        <div className={styles.container}>
            <Tab name={'Publiques'} value={'public'} isSelected={tab === 'public'} select={() => switchPlaylistTab(PlaylistTab.PUBLIC)(dispatch)} />
            { user.isAuth && (
                <Tab name={'Sauvegardées'} value={'saved'} isSelected={tab === 'saved'}
                  select={() => switchPlaylistTab(PlaylistTab.SAVED)(dispatch)}/>
            )}
            { user.isAuth && (
                <Tab name={'Mes Playlists'} value={'mine'} isSelected={tab === 'mine'} select={() => switchPlaylistTab(PlaylistTab.MINE)(dispatch)} />
            )}
        </div>
    );
}

const Tab = ({name, value, isSelected, select}: tabProps) => {
    const props = useSpring({
        width: isSelected ? '100%' :'0%',
        config: {duration: 150}
    })
    return(
        <div className={cn(styles.tab, isSelected && styles.selected)} onClick={() => select?.(value)}>
            <span className={cn( isSelected && styles.selected)} title={name}>{name}</span>
            <animated.div className={styles.underline} style={{width: props.width}} />
        </div>
    )
}

export default Tabs;
