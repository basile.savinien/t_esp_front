import {Playlist} from "@common/types/playlists/playlists";
import Fab from "@src/design_system/fab/fab";
import styles from "./action_playlist_styles.module.css";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {Tooltip} from "antd";
import {HTMLProps, useRef, useState} from "react";
import DropdownAction from "@src/design_system/dropdown_action/dropdown_action";
import {useStore} from "@hooks/redux/useStore";
import {savePlaylist} from "@common/actions/playlists";
import {useDispatch} from "react-redux";
import useIsSaved from "@hooks/playlists/useIsSaved";
import DeletePlaylistModal from "@components/playlists/modals/delete_playlist/delete_playlist";
import {useHistory} from "react-router-dom";

interface PropsButton extends HTMLProps<HTMLDivElement>{
    playlist: Playlist

}

export const ActionPlaylist = ({playlist, ...props}: PropsButton) => {
    const dispatch = useDispatch();
    const history = useHistory();
    const user = useStore(store => store.user);
    const isSaved = useIsSaved(playlist._id);
    const [openDelete, setOpenDelete] = useState(false);


    const onClick = (event: React.MouseEvent<HTMLElement>) => {
        event.stopPropagation();
    }
    if (user?._id !== playlist.creator || !user.isAuth) return null

    return(
        <>
            <div {...props}>
                <DropdownAction
                    component={
                        <Tooltip title="Actions" placement="topRight">
                            <div onClick={onClick}>
                                <Fab
                                    variant={"transparent"}
                                    classNameContainer={styles.action}
                                    icon={
                                        <FontAwesomeIcon
                                            icon={"ellipsis-vertical"}
                                            size={"lg"}
                                            color={'#aaa'}
                                        />
                                    }
                                />
                            </div>
                        </Tooltip>
                    }
                    options={[
                        {
                            text: isSaved ? "Oublier" :"Sauvegarder",
                            action: () => {
                                savePlaylist(playlist._id)(dispatch)
                            }
                        },
                        {
                            text: "Modifier",
                            action: () => {
                                history.push(`/playlist/edit/${playlist._id}`);
                            }
                        },
                        {
                            text: 'Supprimer',
                            action: () => {
                                setOpenDelete( prevState => !prevState);
                            }
                        }
                    ]}
                />

            </div>
            <DeletePlaylistModal playlist={playlist} open={openDelete} onClose={()=> setOpenDelete(false)} />
        </>
    )
}
