import {config} from "@common/types/utils/avatar";
import {clotheColor, facialHairColor, hairColor, skinColor} from "@common/types/utils/avatar/avatar";


export const skinColorHex : { [key in skinColor]:string} = {
    [config.skinColor.PALE] : '#FFDBB4',
    [config.skinColor.LIGHT] : '#EDB98A',
    [config.skinColor.TANNED] : '#FD9841',
    [config.skinColor.YELLOW] : '#F8D25C',
    [config.skinColor.BROWN] : '#D08B5B',
    [config.skinColor.DARK_BROWN] : '#AE5D29',
    [config.skinColor.BLACK] : '#614335',
}

export const hairColorHex : { [key in hairColor]:string} =  {
    [config.hairColor.BLONDE]: '#B58143',
    [config.hairColor.BLONDE_GOLDEN]: '#D6B370',
    [config.hairColor.BROWN]: '#724133',
    [config.hairColor.BROWN_DARK]: '#4A312C',
    [config.hairColor.AUBURN]: '#A55728',
    [config.hairColor.BLACK]: '#2C1B18',
    [config.hairColor.BLUE]: '#000fdb',
    [config.hairColor.RED]: '#C93305',
    [config.hairColor.PASTEL_PINK]: '#F59797',
    [config.hairColor.PLATINUM]: '#ECDCBF',
    [config.hairColor.SILVER_GRAY]: '#E8E1E1',
}

export const facialHairColorHex : { [key in facialHairColor]:string} = {
    [config.facialHairColor.BLONDE]: '#B58143',
    [config.facialHairColor.BLONDE_GOLDEN]: '#D6B370',
    [config.facialHairColor.BROWN]: '#724133',
    [config.facialHairColor.BROWN_DARK]: '#4A312C',
    [config.facialHairColor.BLACK]: '#2C1B18',
    [config.facialHairColor.AUBURN]: '#A55728',
    [config.facialHairColor.SILVER_GRAY]: '#E8E1E1',
    [config.facialHairColor.PLATINUM]: '#ECDCBF',
    [config.facialHairColor.RED]: '#C93305',
}

export const clotheColorHex : { [key in clotheColor]:string} = {
    [config.clotheColor.BLACK]: '#262E33',
    [config.clotheColor.WHITE]: '#FFFFFF',
    [config.clotheColor.RED]: '#FF5C5C',
    [config.clotheColor.BLUE_01]: '#65C9FF',
    [config.clotheColor.BLUE_02]: '#5199E4',
    [config.clotheColor.BLUE_03]: '#25557C',
    [config.clotheColor.GRAY_01]: '#E6E6E6',
    [config.clotheColor.GRAY_02]: '#929598',
    [config.clotheColor.HEATHER]: '#3C4F5C',
    [config.clotheColor.PASTEL_BLUE]: '#B1E2FF',
    [config.clotheColor.PASTEL_GREEN]: '#A7FFC4',
    [config.clotheColor.PASTEL_ORANGE]: '#FFDEB5',
    [config.clotheColor.PASTEL_YELLOW]: '#FFFFB1',
    [config.clotheColor.PASTEL_RED]: '#FFAFB9',
    [config.clotheColor.PINK]: '#FF488E',
}

export const hatColorHex : { [key in clotheColor]:string} = {
    [config.hatColor.BLACK]: '#262E33',
    [config.hatColor.WHITE]: '#FFFFFF',
    [config.hatColor.RED]: '#FF5C5C',
    [config.hatColor.BLUE_01]: '#65C9FF',
    [config.hatColor.BLUE_02]: '#5199E4',
    [config.hatColor.BLUE_03]: '#25557C',
    [config.hatColor.GRAY_01]: '#E6E6E6',
    [config.hatColor.GRAY_02]: '#929598',
    [config.hatColor.HEATHER]: '#3C4F5C',
    [config.hatColor.PASTEL_BLUE]: '#B1E2FF',
    [config.hatColor.PASTEL_GREEN]: '#A7FFC4',
    [config.hatColor.PASTEL_ORANGE]: '#FFDEB5',
    [config.hatColor.PASTEL_YELLOW]: '#FFFFB1',
    [config.hatColor.PASTEL_RED]: '#FFAFB9',
    [config.hatColor.PINK]: '#FF488E',
}

