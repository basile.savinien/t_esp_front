import  React, {useState} from 'react'
import Avatar from 'avataaars'
import styles from './avatar_picker.module.css'
import DeletePlaylistModal from "@components/playlists/modals/delete_playlist/delete_playlist";
import Editor from "@components/avatar_picker/editor/editor";
import {AvatarType, config} from '@common/types/utils/avatar';
import {defaultAvatar} from "@common/defaultValue/avatar";
import cn from "classnames";

interface PickerType {
    avatar?: AvatarType
    onEdit?: (avatar: AvatarType) => void
    className?:string
}


const AvatarPicker = ({avatar = defaultAvatar, onEdit, className}: PickerType) => {
    const [openEditor, setOpenEditor] = useState<boolean>(false)

    const onSave = (avatar: AvatarType) => {
        onEdit?.(avatar);
    }

        return(
            <>
            <div className={cn(styles.container, className)} onClick={()=> {setOpenEditor(true)}} >
                <Avatar
                    style={{width: '100%', height: '100%'}}
                    avatarStyle='Circle'
                    {...avatar}
                />
            </div>
                <Editor
                    avatar={avatar}
                    open={openEditor}
                    onSave={onSave}
                    onClose={()=> setOpenEditor(false)}
                />
            </>
        )
}

export default AvatarPicker;
