import {AvatarType, config} from "@common/types/utils/avatar";


export interface configType {
    avatarKey?: keyof AvatarType
    label?: string
    colorKey?: keyof AvatarType | ((value: AvatarType[ keyof AvatarType] | undefined) => keyof AvatarType | undefined)
}

const editorConfig : configType[] = [
    {
        colorKey: 'skinColor',
        label: 'Couleur de peau',
    },
    {
        avatarKey: 'eyeType',
        label: "Type d'yeux",
    },
    {
        avatarKey: 'eyebrowType',
        label: "Type de sourcils",
    },
    {
        avatarKey: 'mouthType',
        label: "Type de bouche",
    },
    {
        avatarKey: 'topType',
        label: "Coiffure",
        colorKey: (key) => {
            if (key && ([

                config.topType.HIJAB,
                config.topType.TURBAN,
                config.topType.WINTER_HAT_1,
                config.topType.WINTER_HAT_2,
                config.topType.WINTER_HAT_3,
                config.topType.WINTER_HAT_4,

            ] as AvatarType[ keyof AvatarType][]).includes(key)){
                return "hatColor";
            }

            if (key && ([
                config.topType.HAT,
                config.topType.NO_HAIR,
                config.topType.EYE_PATCH,

            ] as AvatarType[ keyof AvatarType][]).includes(key)){
                return undefined;
            }

            return 'hairColor';
        }
    },
    {
        avatarKey: 'facialHairType',
        label: "Type de barbe",
        colorKey: "facialHairColor"
    },
    {
        avatarKey: 'clotheType',
        label: "Type d'habits",
        colorKey: "clotheColor"
    },
    {
        avatarKey: 'accessoriesType',
        label: "Lunettes",
    },
];

export default editorConfig;
