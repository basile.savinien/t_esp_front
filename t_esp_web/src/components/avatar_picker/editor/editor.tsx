import styles from './editor.module.css';
import {Modal} from "@mui/material";
import {useSpring, animated} from "react-spring";
import Button from "@src/design_system/button/Button";
import {AvatarType} from "@common/types/utils/avatar";
import React, {useState} from "react";
import Avatar from "avataaars";
import Select from "@src/design_system/select/select";
import {config} from "@common/types/utils/avatar";

import editor_config, {configType} from "@components/avatar_picker/editor/editor_config";
import SelectColor from "@src/design_system/select_color/select_color";
import {clotheColorHex, facialHairColorHex, hairColorHex, skinColorHex, hatColorHex} from "@components/avatar_picker/colors";
import {randomizeAvatar} from "@common/utils/avatar";
import cn from "classnames";

type ModalWithoutChildren = Omit<React.ComponentProps<typeof Modal>, "children">

interface ModalProps extends ModalWithoutChildren {
    avatar: AvatarType
    onSave: (avatar: AvatarType) => void
}


interface Palette {
    skinColorHex: typeof skinColorHex
    hairColorHex: typeof hairColorHex
    hatColorHex: typeof hatColorHex
    facialHairColorHex: typeof facialHairColorHex
    clotheColorHex: typeof clotheColorHex
}

const palette: Palette  = {
    skinColorHex,
    hairColorHex,
    facialHairColorHex,
    clotheColorHex,
    hatColorHex
}


const Editor = ({ avatar, open, onClose = () => {}, onSave, ...props} : ModalProps) => {
    const [newAvatar, setNewAvatar] = useState<AvatarType>(avatar);
    const [previewAvatar, setPreviewAvatar] = useState<AvatarType>(avatar);
    const style = useSpring({
        from: { opacity: 0 },
        to: { opacity: open ? 1 : 0 },
    });

    const handleClose = (event: React.MouseEvent<HTMLElement>) => {
        event.stopPropagation();
        onClose(event, 'backdropClick')
    }

    const handleSave = (event: React.MouseEvent<HTMLElement>) => {
        event.stopPropagation();
        onSave?.(newAvatar);
        onClose(event, 'backdropClick');
    }

    const reset = () => {
        setNewAvatar(avatar);
        setPreviewAvatar(avatar);
    }

    const random = () => {
        const randomAvatar = randomizeAvatar();
        setNewAvatar(randomAvatar);
        setPreviewAvatar(randomAvatar);
    }



    const onChangeField = <K extends keyof AvatarType> (key: K, value: AvatarType[K]) => {
        setNewAvatar((prevState: AvatarType )=> {
            const data = {
                ...prevState,
                [key]: value
            };
            setPreviewAvatar(data)
            return data;
        })
    }

    const onChangePreview = <K extends keyof AvatarType> (key: K, value: AvatarType[K]) => {
        setPreviewAvatar((prevState: AvatarType )=> ({
            ...prevState,
            [key]: value
        }))
    }

    const resetPreview = () => {
        setPreviewAvatar(newAvatar);
    }

    return(
        <Modal
            open={open}
            onClose={handleClose}
            {...props}
        >
            <animated.div style={style} className={styles.container}>
                <h2 className={styles.title}>Editeur d'avatar</h2>
                <div className={styles.content}>
                <div className={styles.avatarContainer}>
                    <Avatar
                        style={{width: '100%', height: '100%'}}
                        avatarStyle='Circle'
                        {...previewAvatar}
                    />
                </div>
                <div className={styles.form}>
                    {
                        editor_config.map((field) => {
                            const value = field.avatarKey && newAvatar[field.avatarKey];
                            const colorKey = typeof field.colorKey === 'function' ? field.colorKey?.(value) : field.colorKey;
                            const colorValue = colorKey && newAvatar[colorKey];

                                return <Field
                                    label={field.label}
                                    avatarKey={field.avatarKey}
                                    colorKey={colorKey}
                                    value={value}
                                    colorValue={colorValue}
                                    onChangeField={onChangeField}
                                    onChangePreview={onChangePreview}
                                    resetPreview={resetPreview}
                                    key={field.label}
                                />;
                            }
                        )
                    }
                </div>
                </div>

                <div className={styles.action}>
                    <div>
                        <Button uppercase onClick={reset} className={styles.button} color={'secondary'} >reset</Button>
                        <Button uppercase onClick={random} className={styles.button} color={'secondary'} >random</Button>
                    </div>
                    <div>
                        <Button onClick={handleClose} className={styles.button} variant={'light'} >Annuler</Button>
                        <Button onClick={handleSave} className={styles.button} color={"primary"}>Sauvegarder</Button>
                    </div>
                </div>
            </animated.div>
        </Modal>

    );
}

interface  FieldType <K extends keyof AvatarType,T extends keyof AvatarType> extends configType{
    colorKey?: keyof AvatarType
    value?: AvatarType[K]
    colorValue?: AvatarType[T]
    onChangeField : (key: K | T, value: AvatarType[K | T] ) => void
    onChangePreview : (key: K | T, value: AvatarType[K | T] ) => void
    resetPreview : () => void
}

const Field = ({
                   avatarKey,
                   colorKey,
                   label,
                   onChangeField,
                   value,
                   colorValue,
                   onChangePreview,
                   resetPreview
            }: FieldType<keyof AvatarType, keyof AvatarType>
) => {
    const paletteName : string = `${colorKey}Hex`;
    const colors = colorKey
        && Object.keys(palette).includes(paletteName)
        && Object.entries(palette[paletteName as keyof Palette])?.map(([key, value])=>({name: key, value}))

    return(
        <div className={styles.inputContainer} key={label} >
            <span>{label}</span>
            <div
                style={{
                    display: 'flex',
                    flexDirection: avatarKey && colorKey ? 'row' : "column",
                    ...avatarKey && colorKey && {alignItems: 'center'}
                }}
            >
                {
                    avatarKey && (
                        <Select
                            options={Object.values(config[avatarKey]).map(field => ({name: field, value: field}))}
                            value={value}
                            onChange={(newValue) => onChangeField(avatarKey, newValue)}
                            inputClassName={styles.input}
                            containerSelect={cn(styles.containerSelect, colorKey && colors && styles.maxWidthSelect)}
                            onEnterOption={(newValue) => onChangePreview(avatarKey, newValue)}
                            onLeaveOption={resetPreview}

                        />
                    )
                }
                {
                    colorKey && colors &&  (
                        <SelectColor
                            options={colors}
                            value={colorValue}
                            onChange={(newValue) => onChangeField(colorKey, newValue.name)}
                            onEnterOption={(newValue) => onChangePreview(colorKey, newValue.name)}
                            onLeaveOption={resetPreview}
                            pickerClassName={styles.picker}
                        />
                    )
                }

            </div>

        </div>
    );
}

export default Editor;
