import styles from "./nav_bar_styles.module.css";
import {HEIGHT_NAV_BAR} from "@src/config";
import Button from "@src/design_system/button/Button";
import {Link} from 'react-router-dom';
import {useStore} from "@hooks/redux/useStore";
import Avatar from 'avataaars'
import DropdownAction from "@src/design_system/dropdown_action/dropdown_action";
import {logout} from "@common/actions/auth";
import {useDispatch} from "react-redux";

const NavBar =  () => {
    const user = useStore(store=> store.user)
    const dispatch = useDispatch();
    return(
        <div className={styles.container} style={{height: HEIGHT_NAV_BAR}}>
            <span className={styles.logo}>Mz.io</span>
            {!user.isAuth ? (
            <Link to={'/login'}>
                <Button
                    className={styles.login}
                    uppercase
                    size={'lg'}
                >
                    Login
                </Button>
            </Link> ) : (
                    <DropdownAction
                        component={
                            <div className={styles.avatarContainer}>
                                <Avatar
                                    style={{width: '65px', height: '65px', backgroundColor: '#fff', borderRadius: '50%'}}
                                    avatarStyle={"Transparent"}
                                    {...user.avatar}
                                />
                            </div>
                        }
                        enableHover={true}
                        options={[{
                            text: 'Logout',
                            action: () => {
                                logout()(dispatch);
                            }
                        }]}
                    />
                )
            }
        </div>
    );
}

export default NavBar;
