import styles from './game.module.css';
import {useStore} from "@hooks/redux/useStore";
import Player from "@components/lobby/player/player";
import Board from "@components/lobby/board/board";
import {usePlayerMusic} from "@src/hooks/usePlayerMusic";
import BoardControl from "@components/lobby/board_control/board_control";

const Game = ( ) => {
    const { data } = useStore(store => store.room);
    usePlayerMusic();

    if(!data) {
        return null;
    }

    return(
        <div className={styles.container}>
            <div className={styles.players}>
                {
                    data.players.map((player) => {
                        return <Player key={player._id} player={player} />
                    })
                }
            </div>
            <div className={styles.right_column}>
                <BoardControl />
                <Board />
            </div>
        </div>
    );
}

export default Game;
