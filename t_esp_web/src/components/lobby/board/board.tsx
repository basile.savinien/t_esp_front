import styles from './board.module.css';
import Input from "@src/design_system/input/input";
import React, {useState} from "react";
import Button from "@src/design_system/button/Button";
import {sendGuess} from "@common/actions/rooms";
import {useStore} from "@hooks/redux/useStore";
import Avatar from "avataaars";



const Board = () => {
    const {socket, data } = useStore(store => store.room);
    const user  = useStore(store => store.user);
    const [guess, setGuess] = useState<string>("");

    const onChangeField = (event : React.ChangeEvent<HTMLInputElement>) => {
        const {value} = event.target;
        setGuess(value);
    }

    const onKeyPress = (event: React.KeyboardEvent<HTMLInputElement>) => {
        if (event.code === "Enter") guessAction();
    }

    const guessAction = () => {
        if (socket) {
            sendGuess( {
                sender: user._id,
                message: guess,
                username: user.username
            },socket);
        }
        setGuess('');
    }

    return(
        <div className={styles.container}>
                <div className={styles.chat}>
                    {
                        data?.chat.map((value, index) => {
                            const avatar = data?.players.find(p => p._id === value.sender)?.avatar;
                            const mine = value.sender === user._id;
                            return (
                                <div
                                    key={`${value.username}_${index}`}
                                    style={{
                                        width: '100%',
                                        display: 'flex',
                                        justifyContent: mine ? 'flex-end' : 'flex-start'
                                    }}
                                >
                                    <div style={{
                                            backgroundColor: '#E9ECEF',
                                            width: 'fit-content',
                                            padding: '10px 20px',
                                            borderRadius: '20px',
                                            display: "flex",
                                            position: 'relative',
                                            maxWidth: '60%'
                                        }}

                                    >
                                        {(index < 5) && <Avatar
                                            avatarStyle={'Circle'}
                                            {...avatar}
                                            style={{
                                                width: '50px',
                                                height: '50px',
                                                position: 'absolute',
                                                ...!mine && {
                                                    left: -5
                                                },
                                                ...mine && {
                                                    right: -5
                                                },
                                                top: -10
                                            }}
                                        />}
                                        <span style={{
                                            ...!mine && {
                                                marginLeft: 30
                                            },
                                            ...mine && {
                                                marginRight: 30
                                            },
                                        }}>
                                            {value.message}
                                        </span>
                                    </div>
                                </div>
                            )
                        })
                    }

            </div>
            <div className={styles.bottomContainer}>
                <Input
                    className={styles.input}
                    value={guess}
                    onChange={onChangeField}
                    containerClassName={styles.inputContainer}
                    onKeyPress={onKeyPress}

                />
                <Button
                    color={'secondary'}
                    variant={'light'}
                    className={styles.send}
                    disabled={!guess.length}
                    onClick={guessAction}

                >Envoyer</Button>
            </div>
        </div>
    );
}

export default Board;
