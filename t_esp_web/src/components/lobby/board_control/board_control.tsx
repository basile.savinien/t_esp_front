import styles from "./board_controle.module.css"
import {Button, Checkbox, Slider} from "@mantine/core";
import {useLocalStorage} from "@mantine/hooks";
import {useStore} from "@hooks/redux/useStore";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {useSpring, animated} from "react-spring";
import {useEffect, useRef} from "react";
import SpotLight from "@components/spotlight/spotlight";
import Avatar from "avataaars";
import {cleanBuffer, replay} from "@common/actions/rooms";

const BoardControl = () => {
    const user = useStore( store => store.user);
    const {volume} = useStore( store => store.player);
    const {data, buffer, socket} = useStore( store => store.room);
    const [volumeLocal, setVolume] = useLocalStorage<number>({key: 'volume', defaultValue: 0.5});
    const [hideRecap, setHideRecap] = useLocalStorage<boolean>({key: 'hide_recap', defaultValue: false});
    const refSpotlight = useRef<any>();

    const overall = data?.config.finished;

    const recap  = data?.players && data?.players.map(( player) => {
        if (overall) return {...player};
        const userFound = data?.currentMusic?.found.find((p) => p._id === player._id );
        return ({...player, score : userFound ? userFound.score : 0})
    }).sort((a,b) => b.score > a.score ? 1 : -1);

    useEffect(() => {
        if (buffer === null && data?.currentMusic?.currentPlayed && !hideRecap) {
            refSpotlight.current.open();
        }
        if(buffer && data?.currentMusic?.currentPlayed  && !hideRecap) {
            refSpotlight.current.close();
        }

    },[buffer]);

    const changeVolume = (value: number) => {
        setVolume(value / 100);
        volume.gain.value = value / 100;
    }

    const onChangeCheckbox = (event : any) => {
        const { checked } = event.target;
        setHideRecap(checked);
    }

    const replayAction = () => {
        cleanBuffer();
        if(socket) replay(socket)

    }

    useEffect(() => {
        if (buffer ) {
            api.start({ width: '100%', config: { duration: (data?.config.time || 0) * 1000},  onRest: () => {
                api.start({width: '0%', config: { duration: 2000}})
                }});
        }
    },[buffer]);

    const [style, api] = useSpring(() => ({width: '0%' }));

    return(
        <div className={styles.container}>
            <div className={styles.firstRow}>
                    <div className={styles.containerProgress}>
                        <animated.div style={style} className={styles.progressBar} />
                    </div>
            </div>
            <div className={styles.secondRow}>
                <div className={styles.volume}>
                    <FontAwesomeIcon
                        icon={"volume-high"}
                        color={ 'var(--secondary-color)'}
                        className={styles.icon}
                    />
                    <Slider
                        color={'secondary'}
                        className={styles.slider}
                        value={volumeLocal * 100}
                        onChange={changeVolume}
                    />
                    <Checkbox
                        label="Cacher le récap automatique"
                        className={styles.checkbox}
                        color={'secondary'}
                        onChange={onChangeCheckbox}
                        checked={hideRecap}
                        styles={{
                            root: { cursor: 'pointer' },
                            inner: { cursor: 'pointer' },
                            label: {
                                cursor: 'pointer',
                                fontFamily: 'var(--primary-font)',
                                fontWeight: 'bold',
                                paddingLeft: '7px'
                            },
                            input: { cursor: 'pointer' },
                            icon: {cursor: 'pointer' },
                        }}
                    />
                    <Button
                        color={'secondary'}
                        variant={'subtle'}
                        onClick={() => refSpotlight.current?.open()}
                        style={{marginLeft: '20px'}}
                    >Open récap</Button>
                </div>

                <div>
                    {data?.currentMusic?.currentPlayed && data?.currentMusic?.nbMusics &&
                        (
                            <span className={styles.musics}>{data?.currentMusic?.currentPlayed} / {data?.currentMusic?.nbMusics} musiques</span>
                        )}
                </div>

            </div>

            <SpotLight ref={refSpotlight} >

                <div className={styles.containerSpotlight}>
                    <h2 className={styles.title}>Récap {overall && ' Global'}</h2>
                    <div className={styles.list}>
                        {
                            recap && recap.map((player, index) => {
                                return(
                                    <div className={styles.playerContainer} key={`${player._id}`}>
                                        <Avatar
                                            style={{width: '100px', height: '100px'}}
                                            avatarStyle='Circle'
                                            {...player.avatar}
                                        />
                                        <span>{player.username}</span>
                                        <span>{player.score} points</span>
                                    </div>
                                )
                            })
                        }
                    </div>
                    <div className={styles.actionContainer}>
                        { user._id === data?.admin
                        && data?.config?.finished
                        &&  (
                            <Button
                                color={'secondary'}
                                className={styles.button}
                                onClick={replayAction}
                            >Rejouer</Button>
                        )
                        }
                        <Button color={'secondary'} className={styles.button} onClick={() => refSpotlight.current?.close()} >Close</Button>
                    </div>
                </div>
            </SpotLight>
        </div>

    );
};

export default BoardControl;
