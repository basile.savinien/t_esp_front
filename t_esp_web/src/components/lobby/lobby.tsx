import { useParams} from "react-router-dom";
import {useEffect, useRef} from "react";
import {connectRoom, connectRoomState, resetRoom} from "@common/actions/rooms";
import {useDispatch} from "react-redux";
import {useStore} from "@hooks/redux/useStore";
import {Socket} from "socket.io-client";
import PreGame from "@components/lobby/pre_game/pre_game";
import Game from "@components/lobby/game/game";

const Lobby = () => {
    const dispatch = useDispatch();
    const {id} = useParams<{id: string}>();
    const {socket, data} = useStore(store => store.room);
    const socketRef = useRef<Socket | null>(socket);
    const user = useStore(store => store.user);
    const accessToken = user.accessToken || localStorage.getItem('accessToken');


    useEffect(()=> {
        if((accessToken && user.isAuth) || !accessToken) {
            connectRoom(id, user)(dispatch);
        }
    },[id, accessToken, user.isAuth]);

    useEffect(()=> {
        if (socket) {
            socketRef.current = socket;
            connectRoomState(socket)(dispatch);
        }
    },[socket])

    useEffect(()=> {
        return () => {
            resetRoom()(dispatch);
            socketRef.current?.disconnect();
        }
    },[])

    if (!data) return null;

    if(!data.config.started) {
        return <PreGame />
    }

    return <Game />;
}

export default Lobby;
