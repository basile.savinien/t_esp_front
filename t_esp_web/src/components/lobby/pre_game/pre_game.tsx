import React, {FC, useEffect, useRef} from "react";
import styles from './pre_game.module.css'
import {useStore} from "@hooks/redux/useStore";
import Select from "@src/design_system/select/select";
import {Room} from "@common/types/room/index";
import {updateConfig} from "@common/actions/rooms";
import SpotLight from "@components/spotlight/spotlight";
import Header from "@components/playlists/header/header";
import Tabs from "@components/playlists/tabs/tabs";
import Card from "@components/playlists/card/card";
import {getPlaylists} from "@common/actions/playlists";
import {useDispatch} from "react-redux";
import {Playlist} from "@common/types/playlists/playlists";
import Avatar from "avataaars";
import cn from "classnames";
import Button from "@src/design_system/button/Button";



const PreGame : FC = () => {
    const dispatch = useDispatch();
    const user = useStore(store => store.user);
    const {data, socket} = useStore(store => store.room)
    const refSpotlight = useRef<any>();

    const filters = useStore(store => store.listPlaylist.filters);
    const tab = useStore(store => store.listPlaylist.tab);
    const playlists = useStore(store => store.listPlaylist);

    const tabContent = playlists?.[tab];

    useEffect(() => {
        getPlaylists(filters, tab)(dispatch)
    },[tab, filters.identifier])

    const updateConfigField = <K extends keyof  Room["config"]>(key: K ,value: Room["config"][K]) => {
        if (data && socket) {
        updateConfig({
            ...data.config,
            [key]: value
            }, socket);
        }
    }

    const selectPlaylist = (playlist : Playlist) => {
        updateConfigField('playlist', playlist._id);
        refSpotlight.current.close();
    }

    const openSpotlight = () => {
        if (user?._id === data?.admin) {
            refSpotlight.current.open();
        }
    }



    return(
    <div className={styles.container}>
    <div className={styles.config}>
    <div className={styles.selectContainer}>
    <span>Temps de réponse</span>
    <Select
            onChange={(value) => updateConfigField('time', value)}
    disabled={user?._id !== data?.admin}
    options={[
    {
    name: "5 secondes",
    value: 5
    },
    {
    name: "10 secondes",
    value: 10
    },
    {
    name: "20 secondes",
    value: 20
    },
    {
    name: "30 secondes",
    value: 30
    },
    {
    name: "45 secondes",
    value: 45
    },
    ]}
    value={data?.config.time}
    inputClassName={styles.select}
    />
</div>
        {data?.config.playlist ? (
<Card
playlist={data?.config.playlist}
        enableDropdown={false}
        enableSave={false}
        action={openSpotlight}
        containerClassName={cn(styles.selectedPlaylist, user?._id !== data?.admin && styles.disabled)}
        />
        ) : (
<div className={styles.playlist}>
<div className={styles.playlistSelector} onClick={openSpotlight}>
<span>ADD PLAYLIST</span>
        </div>
        </div>
        )}
        </div>
<div className={styles.players}>
        {
        data?.players.map((player) => {
        return (
<div key={player._id} className={styles.avatarContainer}>
<Avatar
style={{width: '120px', height: '120px'}}
        avatarStyle='Circle'
        {...player.avatar}
        />
<span>{player.username}</span>
        </div>
        )
        })
        }
        </div>
<Button
className={styles.button}
        color={'secondary'}
        uppercase
        size={'xl'}
        disabled={user?._id !== data?.admin || !data?.config.playlist}
        onClick={() => updateConfigField('started', true)}
        >Start</Button>

<SpotLight ref={refSpotlight}>
<div>
<Tabs />
<Header />
<div className={styles.content}>
{
tabContent.data.map((playlist) => {
return <Card
        action={(event: React.MouseEvent<HTMLDivElement>) => selectPlaylist(playlist)}
key={`${tab}_${playlist._id}`}
playlist={playlist}
enableDropdown={false}
enableSave={false}
/>;
})
}
</div>
        </div>
        </SpotLight>


        </div>
        )
        }

        export default PreGame;
