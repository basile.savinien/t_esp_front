import React, {FC} from "react";
import {Room} from "@common/types/room/index";
import styles from './player.module.css';
import Avatar from "avataaars";
import {useStore} from "@hooks/redux/useStore";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

interface PlayerProps {
    player: Room["players"][0]
}



const Player : FC<PlayerProps> = ({ player}) => {
    const {data} = useStore(store => store.room);
    const hasFound = !!data?.currentMusic?.found?.find((playerFound) => playerFound._id === player._id);
    return(
        <div className={styles.container}>
            <Avatar
                style={{ height: '95px', width: "95px"}}
                avatarStyle={"Circle"}
                {...player.avatar}
            />
            <div className={styles.row}>
                <div className={styles.first_line}>
                    <span>{player.username}</span>
                    {hasFound  && <FontAwesomeIcon className={styles.found_icon} icon={"circle-check"} color={"green"}/>}
                </div>
                <span>Score: {player.score}</span>
            </div>
        </div>
    );
}

export default Player;
