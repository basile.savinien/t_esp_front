import React, {useCallback, useState} from "react";
import styles from "./side_bar_styles.module.css";
import {useSpring, animated} from "react-spring";
import cn from 'classnames';
import LinkRow from "@components/side_bar/link_row/link_row";
import PlaylistSvg from "@assets/side_bar/playlists_svg";
import MusicSvg from "@assets/side_bar/music_svg";
import {WIDTH_SIDE_BAR_CLOSE, WIDTH_SIDE_BAR_OPEN} from "@src/config";
import SettingsSvg from "@assets/side_bar/settings_svg";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {useStore} from "@hooks/redux/useStore";


const SideBar = () => {
    const [isOpen, setIsOpen] = useState(true);
    const [isIconVisible, setIsIconVisible] = useState(false);
    const user = useStore(store => store.user);

    const props = useSpring({ width: isOpen ? WIDTH_SIDE_BAR_OPEN : WIDTH_SIDE_BAR_CLOSE })



    const handleOpen = useCallback(() => {
        setIsOpen(!isOpen)
    },[isOpen])

    const setVisibleCallback = useCallback(() => {
        setIsIconVisible(true)
    },[])

    const setHiddenCallback = useCallback(() => {
        setIsIconVisible(false)
    },[])


    return (
        <animated.div onMouseOver={setVisibleCallback} onMouseLeave={setHiddenCallback} className={styles.container} style={props} >
            <Icon action={handleOpen} isOpen={isOpen} isIconVisible={isIconVisible}  />
            <LinkRow isOpen={isOpen} name={'Rooms'} slug={'/Rooms'} icon={<FontAwesomeIcon
                icon={"gamepad"}
                size={"2x"}
                color={'var(--secondary-color)'}
            />} />
            <LinkRow isOpen={isOpen} name={'Musics'} slug={'/Musics'} icon={<MusicSvg />} />
            <LinkRow isOpen={isOpen} name={'Playlists'} slug={'/playlists'} icon={<PlaylistSvg />} />
            {user.isAuth && <LinkRow isOpen={isOpen} name={'Settings'} slug={'/Settings'} icon={<SettingsSvg/>}/>}
        </animated.div>
    )
}


interface IconType {
    isOpen: boolean,
    isIconVisible: boolean,
    action: () => void
}

const Icon = ( {isOpen, action, isIconVisible}: IconType) => {
    const props = useSpring({ opacity: isIconVisible ? 1 : 0})
    return(
        <animated.div onClick={action} className={styles.iconContainer} style={props} >
            <svg className={cn(styles.iconSvg, isOpen && styles.iconSvgRotate)} xmlns="http://www.w3.org/2000/svg" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 444.819 444.819">
                <path d="M352.025,196.712L165.884,10.848C159.029,3.615,150.469,0,140.187,0c-10.282,0-18.842,3.619-25.697,10.848L92.792,32.264   c-7.044,7.043-10.566,15.604-10.566,25.692c0,9.897,3.521,18.56,10.566,25.981l138.753,138.473L92.786,361.168   c-7.042,7.043-10.564,15.604-10.564,25.693c0,9.896,3.521,18.562,10.564,25.98l21.7,21.413   c7.043,7.043,15.612,10.564,25.697,10.564c10.089,0,18.656-3.521,25.697-10.564l186.145-185.864   c7.046-7.423,10.571-16.084,10.571-25.981C362.597,212.321,359.071,203.755,352.025,196.712z"/>
            </svg>
        </animated.div>
    )
}

export default SideBar
