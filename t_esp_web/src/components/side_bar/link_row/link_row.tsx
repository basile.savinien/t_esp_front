import React, {ReactElement, useState} from "react";
import styles from './link_row_styles.module.css'
import {Link, useHistory, useParams} from 'react-router-dom'
import {WIDTH_SIDE_BAR_CLOSE} from "@src/config";
import {useSpring, animated} from "react-spring";
import { useLocation } from "react-router-dom";
import classNames from "classnames";
import LinkModal from "@components/side_bar/link_row/link_modal/link_modal";
import useLocalStorage from "@src/hooks/useLocalStorage";

interface linkRowType {
    isOpen: boolean,
    name: string,
    slug: string,
    icon?: ReactElement
}

const margin:number = 10;

const LinkRow = ({isOpen, name, slug, icon} : linkRowType) => {
    const location = useLocation()
    const history = useHistory();
    const [isOpenModal, setIsOpenModal] = useState<boolean>(false);
    const props = useSpring({ opacity: isOpen ? 1 : 0})

    const Icon = icon && React.cloneElement(icon, {
        height: 30,
        width: 30
    });
    const isSelected = location.pathname.replaceAll("/","") === slug;

    const goTo =  (slug: string) => {
        const [path, id] = location.pathname.split('/').filter(v => v);
        const showModal = JSON.parse(window.localStorage.getItem('modal_leave_room') || "true");
        if (path === 'rooms' && id !== undefined && showModal) {
            return setIsOpenModal(true);
        }
        return history.push(slug)
    }

    return(
        <div onClick={() => goTo(slug)}  className={classNames(styles.container, isSelected && styles.selected, !isSelected && styles.notSelected)} style={{width:  `calc(100% - ${margin * 2}px) `, margin: `${margin}px ${margin}px 0`}}>
            <div className={styles.iconContainer} style={{width:  `calc(${WIDTH_SIDE_BAR_CLOSE}px - ${margin * 2}px )`}}>
                {Icon}
            </div>
            <animated.div className={styles.nameContainer} style={{...props}}>
                <span className={styles.name}>{name}</span>
            </animated.div>
            <LinkModal
                slug={slug}
                open={isOpenModal}
                onClose={() => setIsOpenModal(false)}

            />
        </div>
    )
}

export default LinkRow;
