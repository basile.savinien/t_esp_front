import styles from './link_modal.module.css';
import {Modal} from "@mui/material";
import {useSpring, animated} from "react-spring";
import Button from "@src/design_system/button/Button";
import {useHistory} from "react-router-dom";
import useLocalStorage from "@src/hooks/useLocalStorage";
import Radio from "@src/design_system/radio/radio";
import React from "react";


type ModalWithoutChildren = Omit<React.ComponentProps<typeof Modal>, "children">

interface ModalProps extends ModalWithoutChildren {
    slug: string
}



const LinkModal = ({slug, open, onClose = () => {}, ...props} : ModalProps) => {
    const history = useHistory();
    const style = useSpring({
        from: { opacity: 0 },
        to: { opacity: open ? 1 : 0 },
    });

    const [showModal, setShowModal] = useLocalStorage<boolean>("modal_leave_room",true);
    const handleClose = (event: React.MouseEvent<HTMLElement>) => {
        event.stopPropagation();
        onClose(event, 'backdropClick')
    }

    const handleMove = (event: React.MouseEvent<HTMLElement>) => {
        event.stopPropagation();
        onClose(event, 'backdropClick')
        history.push(slug)
    }

    const onChangeRadio =  (event: React.ChangeEvent<HTMLInputElement>) => {
        const { checked}  = event.target;
        setShowModal(!checked);
    }

    return(
        <Modal
            open={open}
            onClose={handleClose}
            {...props}
        >
            <animated.div style={style} className={styles.container}>
                <h2 className={styles.title}>Quitter la Room ?  </h2>
                <p className={styles.text}>Êtes-vous sur de vouloir quitter la Room pour aller sur {slug.replace('/', "")} ? </p>
                <Radio
                    value={'modal_leave_room'}
                    type={'checkbox'}
                    label={"Ne plus me demander"}
                    color={'secondary'}
                    checked={!showModal}
                    onChange={onChangeRadio}
                />
                <div className={styles.action}>
                    <Button onClick={handleClose} className={styles.button} variant={'light'} >Annuler</Button>
                    <Button onClick={handleMove} className={styles.button} color={"secondary"}>Continuer</Button>
                </div>
            </animated.div>
        </Modal>

    );
}

export default LinkModal;
