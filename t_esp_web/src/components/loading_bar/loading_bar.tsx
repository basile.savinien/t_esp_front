import styles from "./loading_bar.module.css"

const LoadingBar =  () => {
    return(
        <div className={styles.container} >
            <div className={styles.background}/>
            <div className={styles.loading}/>
        </div>
    );
}

export default LoadingBar;
