import AvatarPicker from "@components/avatar_picker/avatar_picker";
import {defaultAvatar} from "@common/defaultValue/avatar";
import React, {useState} from "react";
import {AvatarType} from "@common/types/utils/avatar";
import Input from "@src/design_system/input/input";
import Button from "@src/design_system/button/Button";
import styles from './create_anonymous_user.module.css'
import {createTempUser} from "@common/actions/user";
import {useDispatch} from "react-redux";

const CreateAnonymousUser = () => {
    const dispatch = useDispatch();
    const [avatar, setAvatar] = useState<AvatarType>(defaultAvatar);
    const [username, setUsername] = useState<string>("");

    const onChangeField = (event : React.ChangeEvent<HTMLInputElement>) => {
        const {value} = event.target;
        setUsername(value);
    }

    const editAvatar = (newAvatar: AvatarType) => {
        setAvatar(newAvatar);
    }

    const join = () => {
        createTempUser({username, avatar})(dispatch)
    }


    return(
        <div className={styles.container}>
            <h1 className={styles.title}>Créer son personnage</h1>
            <div className={styles.content}>
                <AvatarPicker
                    avatar={avatar}
                    onEdit={editAvatar}
                    className={styles.avatar}
                />
            <Input
                value={username}
                onChange={onChangeField}
                title={"username"}
            />

            </div>

            <Button
                className={styles.button}
                onClick={join}
            >
                Rejoindre
            </Button>
        </div>
    );
}

export default CreateAnonymousUser;
