import {FC, forwardRef, useEffect, useImperativeHandle, useRef, useState} from "react";
import {createPortal} from "react-dom";
import styles from './spotlight_styles.module.css';
import {useSpring, animated, to} from "react-spring";
import {ReactJSXElement} from "@emotion/react/types/jsx-namespace";

const rootElm : HTMLDivElement = document.createElement('div');
rootElm.style.cssText = "position:absolute;height:100%;width:100%; z-index: 1000;";

interface SpotlightProps {
    children: ReactJSXElement
}

const SpotLight   = forwardRef<any,SpotlightProps >(({
                                            children,

}, ref) => {
    const container = useRef(rootElm);
    const contentRef = useRef<any>();

    useImperativeHandle(ref, () => ({
        open: () => {
            document.body.insertBefore(container.current, document.getElementById('root'));
            api.start({ opacity:1, scale: 1 });
        },
        close: () => handleClose()
    }));

    useEffect(() => {
        container.current.addEventListener('click',handleClickOutside)
        return () => {
            container.current.removeEventListener('click', handleClickOutside);
            document.body.contains(container.current) && document.body.removeChild(container.current)
        }

    },[]);

    const handleClickOutside = ( event: any) => {
        const {target} = event;
        if(!contentRef.current?.contains(target as Node)) {
            handleClose();
        }
    }

    const [style, api] = useSpring(() => ({ opacity: 0,  scale: 0.95, config: { duration: 100 }, }));

    const handleClose = () => {
        api.start({ opacity:0, scale: 0.95 , onRest: {
            opacity: () => document.body.contains(container.current) && document.body.removeChild(container.current)
            }
        });
    }

    return createPortal(
        <animated.div  style={{opacity: style.opacity}} className={styles.container} >
            <animated.div ref={contentRef}  className={styles.modal} style={{transform:  to([style.scale], (x) => `scale(${x})`)}}>
                {children}
            </animated.div>
        </animated.div>,
        container.current
    )
})

export default SpotLight;
