FROM node:12.7-alpine

WORKDIR /app/front
COPY . /app/front

RUN npm install

RUN cd t_esp_web && npm install && npm install pm2 -g

RUN cd t_esp_web && npm run build

WORKDIR 't_esp_web'

EXPOSE 3000

CMD npm run prod
