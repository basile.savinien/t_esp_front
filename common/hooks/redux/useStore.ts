import { TypedUseSelectorHook, useDispatch, useSelector} from 'react-redux'
import {Dispatch} from "redux";
import {storeType} from "@common/types/store/index";


export const useAppDispatch = () => useDispatch<Dispatch>();
export const useStore: TypedUseSelectorHook<storeType> = useSelector;
