import {useStore} from "@common/hooks/redux/useStore";
import {UserRole} from "@common/types/users";


export const useRights = ( accessRoles : UserRole[] = []) : boolean => {
    const {role} =  useStore(store => store.user);
    return accessRoles.includes(role);
}
