import  {useState, useEffect} from 'react';

const useDelay = (time: number, enabled = true ) => {
    const [hasDone,  setHasDone] = useState(false);

    useEffect(() => {
    const timeout = setTimeout(() => setHasDone(true),time);
    return () => {
        clearTimeout(timeout);
    }
    },[]);

    if (!enabled) {
        return true
    }
    return hasDone
};

export default useDelay;
