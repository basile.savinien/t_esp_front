import {Playlist} from "@common/types/playlists/playlists";
import {useStore} from "@common/hooks/redux/useStore";
import {useEffect, useState} from "react";


const useIsSaved = (id : Playlist["_id"])  => {
    const {playlistSaved} = useStore(store => store.user);
    const [isSaved, setIsSaved] = useState(false);

    useEffect(() => {
        if (playlistSaved?.includes(id)) {
            setIsSaved(true);
        } else {
            setIsSaved(false);
        }
    },[playlistSaved, id]);

    return isSaved;
}

export default useIsSaved;
