import {useEffect, useState} from "react";
import {getPlaylist} from "@common/actions/playlists";
import {Playlist} from "@common/types/playlists/playlists";


const isLoaded = (arg : Playlist | Playlist["_id"] ) : boolean => typeof arg === 'object';

const usePlaylist = (arg : Playlist["_id"] | Playlist) :  Playlist | null => {
    const [data, setData] = useState(isLoaded(arg) ? arg as Playlist : null);

    useEffect(() => {
        if (!isLoaded(arg)) {
            getPlaylist(arg as string).then((res) => {
                setData(res);
            });
        }
    },[typeof arg === 'string' ? arg : arg?._id])

    return data;
}

export default usePlaylist;
