import {useEffect, useState} from "react";
import {getMusic} from "@common/actions/musics";
import {Music} from "@common/types/musics/musics";


const isLoaded = (arg : Music | Music["_id"] ) : boolean => typeof arg === 'object';

 const useMusic = (arg : Music["_id"] | Music) :  Music | null => {
     const [data, setData] = useState(isLoaded(arg) ? arg as Music : null);

     useEffect(() => {
         if (!isLoaded(arg)) {
             getMusic(arg as string).then((res) => {
                 setData(res);
             });
         }
     },[])

    return data;
}

export default useMusic;
