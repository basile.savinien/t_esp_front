import {Music} from "@common/types/musics/musics";


export const enum PlaylistType {
    MOVIE = 'movie',
    SERIE = 'serie',
    MUSIC = 'music',
    ANIME = 'anime',
    CARTOON = 'cartoon',
    MIXTE = 'mixte'
}

export interface Playlist {
    name: string
    musics: Music['_id'][]
    type: PlaylistType
    creator: string
    private: boolean
    length: number
    _id: string
}

export const enum PlaylistTab {
    PUBLIC = 'public',
    MINE = 'mine',
    SAVED = 'saved'
}

export interface Playlists {
    data: Playlist[]
    identifier: string | null
}


export interface FiltersPlaylist {
    tab: PlaylistTab
    name: string
    range: {
        min: number | null
        max: number | null
    }
    type: PlaylistType | 'all'
    identifier: string
}

