export enum  MusicType  {
    MOVIE = 'movie',
    SERIE = 'serie',
    MUSIC = 'music',
    ANIME = 'anime',
    CARTOON = 'cartoon',
}

export interface Music {
    title: string
    artists?: string[]
    type: MusicType
    keywords: string[]
    _id: string
}

export interface FiltersMusic {
    title: string
    artists: []
    type: MusicType | 'all'
    identifier: string
}

