import {userType} from "@common/types/users/users";
import {Playlist} from "@common/types/playlists/playlists";
import {Music} from "@common/types/musics/musics";

type Score = number;

interface Player extends Pick<userType, "_id" | "username" | "avatar"> {
    score: Score
}

interface ConfigRoom {
    started: boolean
    time: number
    playlist: Playlist["_id"] | null
    finished: boolean
}

interface Message {
    sender: Player["_id"]
    message: string
    username: Player["username"]
}

interface CurrentMusic {
    _id?: Music["_id"]
    timestamp?: number
    found: Player[]
    currentPlayed?: number
    nbMusics?: number
}

export interface Room {
    players: Player[]
    private: boolean
    admin: userType["_id"]
    config: ConfigRoom
    chat: Message[]
    currentMusic:  CurrentMusic
    _id: string
}
