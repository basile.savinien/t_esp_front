import {Playlist} from "@common/types/playlists";
import {AvatarType} from "@common/types/utils/avatar";
import {formField} from "@common/types/utils/form";

export interface userType {
    isAuth: boolean;
    email?: string;
    username?: string;
    role: UserRole;
    playlistSaved?: Playlist["_id"][]
    accessToken?:string;
    hasFetch?: boolean;
    _id?: string
    avatar?: AvatarType
}

export const enum UserRole {
    ADMIN = 'admin',
    MODERATOR = 'moderator',
    USER = 'user',
    GUEST = 'guest'
}

export interface formSetting {
    username: formField<string>
    email: formField<string>
    oldPassword: formField<string>
    password: formField<string>
    avatar: formField<AvatarType>
}
