import {MusicType} from "@common/types/musics";

export interface createMusicType {
    form: createMusicForm
    error: createMusicError
}

export interface createMusicForm {
    title: string
    authors?: string[]
    type: MusicType
    keywords: string[]
    file: File | undefined
}

export interface createMusicError {
    title: errorForm
    authors: errorForm
    type: errorForm
    keywords: errorForm
    file: errorForm
}

export interface errorForm {
    message:string
    bool: boolean
}
