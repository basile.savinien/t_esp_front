import {errorForm} from "@common/types/store/reducer/createMusicReducer";
import {ListMusic} from "@common/types/store/reducer/listMusicsReducer";
import {Music} from "@common/types/musics";

export interface editPlaylistType {
    id: string | null
    form: editPlaylistForm
    error: editPlaylistError
    search: ListMusic
}

export interface createPlaylistType {
    form: editPlaylistForm
    error: editPlaylistError
    search: ListMusic
}

export interface editPlaylistForm {
    name: string
    private: boolean
    musics: Music[]
}

export interface editPlaylistError {
    name: errorForm
    musics: errorForm
}
