import {FiltersMusic, Music} from "@common/types/musics";


export interface ListMusic {
    filters: FiltersMusic
    data: Music[]
}
