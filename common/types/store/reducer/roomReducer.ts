import {Room} from "@common/types/room/index";
import {Socket} from "socket.io-client";


export interface RoomReducer {
    data: Room | null
    socket: Socket | null
    buffer: Buffer[] | null
}
