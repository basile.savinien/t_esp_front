import {FiltersPlaylist, Playlists, PlaylistTab} from "@common/types/playlists";



export interface ListPlaylist {
    filters: FiltersPlaylist
    tab: PlaylistTab
    public: Playlists
    saved: Playlists
    mine: Playlists
}
