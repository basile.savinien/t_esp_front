import {createMusicType} from "./reducer/createMusicReducer";
import { editPlaylistType} from "./reducer/editPlaylistReducer";
import {ListMusic} from "./reducer/listMusicsReducer";
import {ListPlaylist} from "./reducer/listPlaylistsReducer";
import {userType} from "@common/types/users";
import {RoomReducer} from "@common/types/store/reducer/roomReducer";
import {Player} from "@common/types/player/index";

export interface storeType {
    user: userType
    createMusic: createMusicType
    listMusic: ListMusic
    editPlaylist: editPlaylistType
    listPlaylist: ListPlaylist
    room: RoomReducer
    player: Player
}
