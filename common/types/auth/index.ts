import {AvatarType} from "@common/types/utils/avatar";

export interface formLogin {
    email: string;
    password: string;
}

export interface formRegister {
    email: string;
    username: string;
    password: string;
    avatar: AvatarType
}
