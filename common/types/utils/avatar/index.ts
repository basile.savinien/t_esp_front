import type {AvatarType, ColorKey} from "./avatar";
import {
    accessoriesType, clotheColor,
    clotheType,
    eyebrowType, eyeType, facialHairColor,
    facialHairType,
    hairColor, hatColor,
    mouthType,
    skinColor,
    topType
} from "./avatar";


const config = {
    skinColor,
    mouthType,
    eyebrowType,
    topType,
    accessoriesType,
    hairColor,
    hatColor,
    facialHairType,
    facialHairColor,
    clotheType,
    clotheColor,
    eyeType
}

export {
    AvatarType,
    ColorKey,
    config
}
