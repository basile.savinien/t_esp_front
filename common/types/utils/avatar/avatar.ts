

export interface AvatarType {
    topType: topType
    accessoriesType: accessoriesType
    hairColor: hairColor
    hatColor: hatColor
    facialHairType: facialHairType
    facialHairColor:facialHairColor
    clotheType: clotheType
    clotheColor: clotheColor
    eyeType: eyeType
    eyebrowType: eyebrowType
    mouthType: mouthType
    skinColor: skinColor
}

export interface ColorKey {
    skinColor: skinColor
    hairColor: hairColor
    facialHairColor: facialHairColor
    clotheColor: clotheColor
    hatColor: hairColor
}


export enum  skinColor  {
    TANNED = 'Tanned',
    YELLOW = 'Yellow',
    PALE = 'Pale',
    LIGHT = 'Light',
    BROWN = 'Brown',
    DARK_BROWN = 'DarkBrown',
    BLACK = 'Black',
}

export enum  mouthType  {
    CONCERNED = 'Concerned',
    DISBELIEF = 'Disbelief',
    EATING = 'Eating',
    GRIMACE = 'Grimace',
    SAD = 'Sad',
    SCREAM_OPEN = 'ScreamOpen',
    SERIOUS = 'Serious',
    SMILE = 'Smile',
    TONGUE = 'Tongue',
    VOMIT = 'Vomit',
    DEFAULT = 'Default',
}
export enum  eyebrowType  {
    DEFAULT = 'Default',
    DEFAULT_NATURAL = 'DefaultNatural',
    ANGRY = 'Angry',
    ANGRY_NATURAL = 'AngryNatural',
    FLAT_NATURAL = 'FlatNatural',
    RAISED_EXCITED = 'RaisedExcited',
    RAISED_EXCITED_NATURAL = 'RaisedExcitedNatural',
    SAD_CONCERNED = 'SadConcerned',
    SAD_CONCERNED_NATURAL = 'SadConcernedNatural',
    UNI_BROW_NATURAL = 'UnibrowNatural',
    UP_DOWN = 'UpDown',
    UP_DOWN_NATURAL = 'UpDownNatural',
}

export enum eyeType {
    DEFAULT = 'Default',
    CLOSE = 'Close',
    CRY = 'Cry',
    DIZZY = 'Dizzy',
    EYE_ROLL='EyeRoll',
    HAPPY = 'Happy',
    HEARTS = 'Hearts',
    SIDE = 'Side',
    SQUINT='Squint',
    SURPRISED='Surprised',
    WINK = 'Wink',
    WINK_WACKY='WinkWacky'
}

export enum clotheColor {
    BLACK ='Black',
    BLUE_01 ='Blue01',
    BLUE_02 ='Blue02',
    BLUE_03 ='Blue03',
    GRAY_01 ='Gray01',
    GRAY_02 ='Gray02',
    HEATHER ='Heather',
    PASTEL_BLUE ='PastelBlue',
    PASTEL_GREEN ='PastelGreen',
    PASTEL_ORANGE ='PastelOrange',
    PASTEL_RED ='PastelRed',
    PASTEL_YELLOW ='PastelYellow',
    PINK ='Pink',
    RED ='Red',
    WHITE ='White',
}

export enum clotheType {
    BLAZER_SHIRT ='BlazerShirt',
    BLAZER_SWEATER ='BlazerSweater',
    COLLAR_SWEATER ='CollarSweater',
    GRAPHIC_SHIRT ='GraphicShirt',
    HOODIE ='Hoodie',
    OVERALL ='Overall',
    SHIRT_CREW_NECK ='ShirtCrewNeck',
    SHIRT_SCOOP_NECK ='ShirtScoopNeck',
    SHIRT_V_NECK ='ShirtVNeck',
}

export enum  facialHairType {
    BLACK ='Blank',
    BEARD_MEDIUM ='BeardMedium',
    BEARD_LIGHT ='BeardLight',
    BEARD_MAJESTIC ='BeardMajestic',
    MOUSTACHE_FANCY ='MoustacheFancy',
    MOUSTACHE_MAGNUM ='MoustacheMagnum',

}

export enum facialHairColor {
    AUBURN ='Auburn',
    BLACK ='Black',
    BLONDE ='Blonde',
    BLONDE_GOLDEN ='BlondeGolden',
    BROWN ='Brown',
    BROWN_DARK ='BrownDark',
    PLATINUM ='Platinum',
    RED ='Red',
    SILVER_GRAY ='SilverGray',
}

export enum accessoriesType {
    BLANK = 'Blank',
    KURT='Kurt',
    PRESCRIPTION_01='Prescription01',
    PRESCRIPTION_02='Prescription02',
    ROUND='Round',
    SUNGLASSES='Sunglasses',
    WAYFARERS='Wayfarers',
}

export enum topType {
    EYE_PATCH='Eyepatch',
    HAT ='Hat',
    HIJAB ='Hijab',
    LONG_HAIR_BIG_HAIR ='LongHairBigHair',
    LONG_HAIR_BOB ='LongHairBob',
    LONG_HAIR_BUN ='LongHairBun',
    LONG_HAIR_CURLY ='LongHairCurly',
    LONG_HAIR_CURVY ='LongHairCurvy',
    LONG_HAIR_DREADS ='LongHairDreads',
    LONG_HAIR_FRIDA ='LongHairFrida',
    LONG_HAIR_FRO ='LongHairFro',
    LONG_HAIR_FRO_BAND ='LongHairFroBand',
    LONG_HAIR_MIA_WALLACE ='LongHairMiaWallace',
    LONG_HAIR_SHAVED_SIDES ='LongHairShavedSides',
    LONG_HAIR_STRAIGHT ='LongHairStraight',
    LONG_HAIR_STRAIGHT_2 ='LongHairStraight2',
    LONG_HAIR_STRAIGHT_STRAND ='LongHairStraightStrand',
    NO_HAIR ='NoHair',
    SHORT_HAIR_DREAD_01 ='ShortHairDreads01',
    SHORT_HAIR_DREAD_02 ='ShortHairDreads02',
    SHORT_HAIR_FRIZZLE ='ShortHairFrizzle',
    SHORT_HAIR_SHAGGY_MULLET ='ShortHairShaggyMullet',
    SHORT_HAIR_SHORT_CURLY ='ShortHairShortCurly',
    SHORT_HAIR_SHORT_FLAT ='ShortHairShortFlat',
    SHORT_HAIR_SHORT_ROUND ='ShortHairShortRound',
    SHORT_HAIR_SHORT_WAVED ='ShortHairShortWaved',
    SHORT_HAIR_SIDES ='ShortHairSides',
    SHORT_HAIR_THE_CAESAR ='ShortHairTheCaesar',
    SHORT_HAIR_THE_CAESAR_SIDE_PART ='ShortHairTheCaesarSidePart',
    TURBAN ='Turban',
    WINTER_HAT_1 ='WinterHat1',
    WINTER_HAT_2 ='WinterHat2',
    WINTER_HAT_3 ='WinterHat3',
    WINTER_HAT_4 ='WinterHat4',
}

export enum hairColor {
    AUBURN ='Auburn',
    BLACK ='Black',
    BLONDE ='Blonde',
    BLONDE_GOLDEN ='BlondeGolden',
    BROWN ='Brown',
    BROWN_DARK ='BrownDark',
    PASTEL_PINK = 'PastelPink',
    BLUE = 'blue',
    PLATINUM ='Platinum',
    RED ='Red',
    SILVER_GRAY ='SilverGray',
}
export enum hatColor {
    BLACK ='Black',
    BLUE_01 ='Blue01',
    BLUE_02 ='Blue02',
    BLUE_03 ='Blue03',
    GRAY_01 ='Gray01',
    GRAY_02 ='Gray02',
    HEATHER ='Heather',
    PASTEL_BLUE ='PastelBlue',
    PASTEL_GREEN ='PastelGreen',
    PASTEL_ORANGE ='PastelOrange',
    PASTEL_RED ='PastelRed',
    PASTEL_YELLOW ='PastelYellow',
    PINK ='Pink',
    RED ='Red',
    WHITE ='White',
}
