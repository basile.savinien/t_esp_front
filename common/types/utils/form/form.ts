
export interface formField<Type> {
    value: Type
    error: {
        bool: boolean
        message: string
    }
}


export interface formEnumToType {
    string: string
    number: number
    object: object
}

export type typeForm = string | number | object;

export const enum formType {
    STRING= 'string',
    NUMBER = 'number',
    OBJECT = 'object'
}

