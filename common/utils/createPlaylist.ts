import {Music} from "@common/types/musics/musics";
import {editPlaylistForm} from "@common/types/store/reducer/editPlaylistReducer";

export const formatFormEditPlaylist = (form: editPlaylistForm):any => {
    const type = getPlaylistType(form.musics);
    return({
        name: form.name,
        isPrivate: form.private,
        musics: form.musics.map((music) => music._id),
        type
    })
}

const getPlaylistType = (musics: Music[]) => {
    let type = null;
    for (let index = 0; index < musics.length; index++) {
        const music = musics[index];
        if (type === null ) {
            type = music.type;
            continue;
        }
        if (type === music.type) {
            continue;
        }
        type = 'mixte';
        break;
    }
    return type;
}
