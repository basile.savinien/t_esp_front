import {AvatarType, config} from "@common/types/utils/avatar";
import {defaultAvatar} from "@common/defaultValue/avatar";


export const randomizeAvatar = () : AvatarType =>
    Object.entries(config).reduce(( acc, [key,value])=>  {
        const properties = Object.values(value);
        return {...
            acc,
            [key]: properties[Math.floor(Math.random() * properties.length)]
        }
    },defaultAvatar);
