
export const formatFormCreateMusic = (form: createMusicForm):any => {
    return({
        title: form.title,
        artists: form.authors,
        keywords: form.keywords,
        type: form.type
    })
}
