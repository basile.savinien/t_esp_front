import {AvatarType, config} from "@common/types/utils/avatar";

export const defaultAvatar : AvatarType = {
    skinColor: config.skinColor.PALE,
    mouthType: config.mouthType.DEFAULT,
    eyebrowType: config.eyebrowType.DEFAULT,
    topType: config.topType.SHORT_HAIR_SHORT_FLAT,
    accessoriesType: config.accessoriesType.PRESCRIPTION_02,
    hairColor:config.hairColor.BROWN,
    hatColor:config.hatColor.RED,
    facialHairType: config.facialHairType.BEARD_LIGHT,
    clotheType: config.clotheType.HOODIE,
    clotheColor: config.clotheColor.BLACK,
    eyeType: config.eyeType.DEFAULT,
    facialHairColor: config.facialHairColor.BROWN
}
