import {v4 as uuid} from "uuid";
import {FiltersPlaylist, PlaylistTab} from "@common/types/playlists/playlists";

export const initialSearch : FiltersPlaylist = {
    tab: PlaylistTab.PUBLIC,
    name: '',
    type: 'all',
    range: {
        min: 0,
        max: 30
    },
    identifier: uuid()
}
