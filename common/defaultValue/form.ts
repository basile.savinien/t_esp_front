import {formField, formType} from "@common/types/utils/form";
import {errorForm} from "@common/types/store/reducer/createMusicReducer";


export const formFieldDefault = (type: formType = formType.STRING, defaultValue : any = undefined ) : formField<any> => {
    switch (type) {
        case formType.STRING:
            return({
                value: defaultValue || "",
                error: errorDefault
            })
        case formType.NUMBER:
            return({
                value: defaultValue || 0,
                error: errorDefault
            })
        case formType.OBJECT:
            return({
                value:  defaultValue || {},
                error: errorDefault
            })
        default:
            return({
                value: defaultValue || "",
                error: errorDefault
            })
    }
}

export const errorDefault : errorForm = {
    bool: false,
    message: ""
}
