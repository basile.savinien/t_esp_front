import {Dispatch} from "redux";
import axios from 'axios';
import {RESET_FIELDS_CREATE_MUSIC, UPDATE_FIELD_CREATE_MUSIC, RESET_FIELD_CREATE_MUSIC} from "@storeType/index";
import {formatFormCreateMusic} from "@common/utils/createMusic"
import {createMusicForm} from "@common/types/store/reducer/createMusicReducer";

export const updateField = <K extends keyof createMusicForm> (name : K, value: createMusicForm[K]) => (dispatch: Dispatch) => {
    dispatch({
        type: UPDATE_FIELD_CREATE_MUSIC,
        payload: {
            field:name,
            value
        }
    })
}

export const resetField =  (name : keyof createMusicForm) => (dispatch: Dispatch) => {
    dispatch({
        type: RESET_FIELD_CREATE_MUSIC,
        payload: name
    })
}


export const createMusic = (form: createMusicForm) => (dispatch: Dispatch) => {
    const musicData = formatFormCreateMusic(form);
    const formData = new FormData();
    if (form.file) formData.append('file', form.file);
    formData.append('musicData', JSON.stringify(musicData));
    return axios.post(`${process.env.REACT_APP_API}/musics`,formData,{
        headers: {
            'Access-Control-Allow-Origin' : '*',
            'Content-Type': 'multipart/form-data',
        }
    }).then((res)=> {
        if (res.status === 200) {
            dispatch({
                type: RESET_FIELDS_CREATE_MUSIC
            })
        }
    });
}
