import {Dispatch} from "redux";
import {
    RESET_FIELDS_CREATE_PLAYLIST, RESET_FIELDS_EDIT_PLAYLIST
} from "@storeType/index";
import axios from "axios";
import { formatFormEditPlaylist} from "@common/utils/createPlaylist";
import {editPlaylistForm} from "@common/types/store/reducer/editPlaylistReducer";


export const createPlaylist = (form: editPlaylistForm) => (dispatch: Dispatch) => {
    const playlistData = formatFormEditPlaylist(form);
    return axios.post(`${process.env.REACT_APP_API}/playlists`,playlistData,{
        headers: {
            'Access-Control-Allow-Origin' : '*',
        }
    }).then((res)=> {
        if (res.status === 200) {
            dispatch({
                type: RESET_FIELDS_EDIT_PLAYLIST
            })
        }
    });
}
