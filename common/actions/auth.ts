import {Dispatch} from "redux";
import axios from "axios";
import {FETCH_USER, LOGOUT} from "@storeType/index";
import {formLogin, formRegister} from "@common/types/auth/index";
import {userType} from "@common/types/users/users";

export const login = (form: formLogin) => (dispatch: Dispatch) => {
    return axios.post<userType>(`${process.env.REACT_APP_API}/login`,{...form}).then((res)=> {
        if(res.status === 200) {
             dispatch({
                type: FETCH_USER,
                payload: {
                    ...res.data,
                }
            })
            if(res?.data?.accessToken) {
                localStorage.setItem("accessToken", res.data?.accessToken);
                axios.defaults.headers.common = {
                    'Authorization': `Bearer ${res.data?.accessToken}`
                };
            }
            return({
            ...res.data,
                isAuth: true
            })
        }
        dispatch({
            type: LOGOUT
        });
        localStorage.removeItem('accessToken');
        delete axios.defaults.headers.common["Authorization"];
        throw res.data;
    }).catch((err) => {
        dispatch({
            type: LOGOUT
        });
        localStorage.removeItem('accessToken');
        delete axios.defaults.headers.common["Authorization"];
    })
};

export const register = (form: formRegister) => (dispatch: Dispatch) => {
    return axios.post(`${process.env.REACT_APP_API}/register`,{...form}).then((res)=> {
        if (res.status === 201 || res.status === 200) {
            return login(form)(dispatch);
        }
        throw res.data;
    })
}

export const logout = () =>  (dispatch: Dispatch) => {
    dispatch({
        type: LOGOUT
    })
    localStorage.removeItem('accessToken');
    return({success: true})

}
