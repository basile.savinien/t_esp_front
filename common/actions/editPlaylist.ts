import {Dispatch} from "redux";
import {
    RESET_FIELDS_EDIT_PLAYLIST,
    REMOVE_MUSIC_TO_EDIT_PLAYLIST,
    ADD_MUSIC_TO_EDIT_PLAYLIST,
    UPDATE_FIELD_EDIT_PLAYLIST,
    UPDATE_FIELDS_EDIT_PLAYLIST,
    UPDATE_FILTER_EDIT_PLAYLIST
} from "@storeType/index";
import axios from "axios";
import { formatFormEditPlaylist} from "@common/utils/createPlaylist";
import {FiltersMusic, Music} from "@common/types/musics/musics";
import {editPlaylistForm} from "@common/types/store/reducer/editPlaylistReducer";
import {Playlist} from "@common/types/playlists/playlists";
import {getPlaylist} from "@common/actions/playlists";


export const initializeEdit = (id : Playlist["_id"] | null = null) => async (dispatch: Dispatch) => {
    if (!id){
        return dispatch({
            type: RESET_FIELDS_EDIT_PLAYLIST,
            id
        })
    }

    return getPlaylist(id, true).then((playlist) => {
        dispatch({
            type: RESET_FIELDS_EDIT_PLAYLIST,
            id
        });
        return dispatch({
            type: UPDATE_FIELDS_EDIT_PLAYLIST,
            payload: {
                name: playlist.name,
                private: playlist.private,
                musics: playlist.musics
            }
        })
    })
}

export const updateFiltersEditPlaylist = (filters: {[key in keyof FiltersMusic ]?:  FiltersMusic[key]}) => (dispatch: Dispatch) => {
    dispatch({
        type: UPDATE_FILTER_EDIT_PLAYLIST,
        payload: filters
    })
}

export const updateFieldEditPlaylist = <K extends keyof editPlaylistForm> (name : K, value: editPlaylistForm[K]) => (dispatch: Dispatch) => {
    dispatch({
        type: UPDATE_FIELD_EDIT_PLAYLIST,
        payload: {
            field: name,
            value
        }
    })
}

export const addMusicToEditPlaylist = (music: Music) => (dispatch: Dispatch) => {
    dispatch({
        type: ADD_MUSIC_TO_EDIT_PLAYLIST,
        payload: music
    })
}


export const removeMusicToEditPlaylist = (music: Music) => (dispatch: Dispatch) => {
    dispatch({
        type: REMOVE_MUSIC_TO_EDIT_PLAYLIST,
        payload: music
    })
}

export const editPlaylist = (id: Playlist["_id"] ,form: editPlaylistForm) => (dispatch: Dispatch) => {
    const playlistData = formatFormEditPlaylist(form);
    return axios.patch(`${process.env.REACT_APP_API}/playlists/${id}`,playlistData,{
        headers: {
            'Access-Control-Allow-Origin' : '*',
        }
    }).then((res)=> {
        if (res.status === 200) {
            return;
        }
    });
}
