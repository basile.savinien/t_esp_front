import {Dispatch} from "redux";
import {SWITCH_TAB_PLAYLIST, UPDATE_FILTER_PLAYLIST, UPDATE_LIST_PLAYLIST, UPDATE_USER, DELETE_PLAYLIST} from "@storeType/index";
import axios from "axios";
import {initialSearch} from "@common/defaultValue/playlist";
import {FiltersPlaylist, Playlist, PlaylistTab} from "@common/types/playlists/playlists";


interface ParamsFilter extends Partial<FiltersPlaylist> {
    rangeMax?: number | null
    rangeMin?: number | null
}

export const updateFiltersPlaylists = (filters: {[key in keyof FiltersPlaylist ]?:  FiltersPlaylist[key]}) => (dispatch: Dispatch) => {
    dispatch({
        type: UPDATE_FILTER_PLAYLIST,
        payload: filters
    })
}

export const switchPlaylistTab = (tab: PlaylistTab) => (dispatch: Dispatch) => {
    dispatch({
        type: SWITCH_TAB_PLAYLIST,
        payload: tab
    })
}

const playlistsRoutes = {
    public: '',
    mine: 'me',
    saved: 'saved'
}

export const getPlaylists = (filters: FiltersPlaylist,tab: 'public' | 'mine' | 'saved') => (dispatch: Dispatch)  => {
    const params : ParamsFilter = {
        ...filters
    };
    delete params.identifier;
    delete params.tab;
    if (params.type === 'all') delete params.type;
    if (!params.name?.length) delete params.name;
    if (params.range?.min !== initialSearch.range.min || params.range?.max !== initialSearch.range.max ){
        params.rangeMin = params.range?.min;
        params.rangeMax = params.range?.max;
    }
    delete params.range;

    return axios.get<Playlist[]>(`${process.env.REACT_APP_API}/playlists/${playlistsRoutes?.[tab]}`,{
        params
    }).then((res)=> {
        if (res.status === 200) {
            dispatch({
                type: UPDATE_LIST_PLAYLIST,
                payload: {
                    tab: tab,
                    data: res.data,
                    identifier: Object.keys(params).length ? filters.identifier : null
                }
            })
            return res.data;
        }
        throw res;
    });
}

export const savePlaylist = (id: Playlist["_id"]) => (dispatch: Dispatch)  => {
    return axios.post(`${process.env.REACT_APP_API}/playlists/save/${id}`).then((res) => {
        if (res.status === 200) {
            dispatch({
                type : UPDATE_USER,
                payload: res.data
            })
            return res.data;
        }
        throw res;
    })
}


export const deletePlaylist = (id: Playlist["_id"]) => (dispatch: Dispatch) => {
    return axios.delete(`${process.env.REACT_APP_API}/playlists/${id}`).then((res) => {
        if (res.status === 200) {
            dispatch({
                type: DELETE_PLAYLIST,
                payload: id
            })
            return res.data;
        }
        throw res
    })
}

export const getPlaylist = (id: Playlist["_id"], populate : boolean = false) => {
    return axios.get<Playlist>(`${process.env.REACT_APP_API}/playlists/${id}?populate=${populate}`)
        .then((res) => res.data);
}
