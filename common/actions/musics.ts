import {UPDATE_FILTER_MUSIC, UPDATE_LIST_MUSIC} from "@storeType/index";
import axios from "axios";
import {Dispatch} from 'redux'
import {FiltersMusic, Music} from "@common/types/musics";

export const getMusics = (filters: FiltersMusic) => (dispatch: Dispatch, type: string = UPDATE_LIST_MUSIC)  => {
    const params : {[key in keyof FiltersMusic ]?:  FiltersMusic[key]} = {...filters};
    delete params.identifier;
    if (params.type === 'all') delete params.type;
    if (!params.title?.length) delete params.title;
    if (!params.artists?.length) delete params.artists;

    return axios.get<Music[]>(`${process.env.REACT_APP_API}/musics`,{
        params
    }).then((res)=> {
        if (res.status === 200) {
            dispatch({
                type,
                payload: res.data
            })
            return res.data;
        }
        throw res;
    });
}

export const getMusic = (id: Music["_id"])   => {
    return axios.get<Music>(`${process.env.REACT_APP_API}/musics/${id}`,).then((res)=> {
        if (res.status === 200) {
            return res.data;
        }
        throw res;
    });
}


export const updateFilters = (filters: {[key in keyof FiltersMusic ]?:  FiltersMusic[key]}) => (dispatch: Dispatch) => {
    dispatch({
        type: UPDATE_FILTER_MUSIC,
        payload: filters
    })
}
