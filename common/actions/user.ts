import {Dispatch} from "redux";
import axios from "axios";
import {FETCH_USER, LOGOUT, UPDATE_USER} from "@storeType/index";
import {formSetting, userType} from "@common/types/users";
import { v4 as uuidv4 } from 'uuid';

export const getMe = () => (dispatch: Dispatch) => {
    const accessToken =localStorage.getItem('accessToken')
    if(!accessToken){
        return;
    }

    axios.defaults.headers.common = {
        'Authorization': `Bearer ${accessToken}`
    };
    return axios.get<{user : userType}>(`${process.env.REACT_APP_API}/users/me`).then((res)=> {
        if (res.status === 200) {
            dispatch({
                type: FETCH_USER,
                payload: {
                    ...res.data.user,
                    accessToken
                }
            });
            return({
                ...res.data,
                isAuth: true,
                hasFetch: true
            });
        }
        localStorage.removeItem('accessToken');
        dispatch({
            type: LOGOUT
        })
        return res;
    }).catch(err=> {
        console.log(err)
        localStorage.removeItem('accessToken');
        dispatch({
            type: LOGOUT
        })
    });
}

export const editMe = (data: Partial<userType>) => (dispatch: Dispatch) => {
    return axios.patch<{user : userType}>(`${process.env.REACT_APP_API}/users/me`, data)
        .then((res) => {
            if (res.status === 200) {
                dispatch({
                    type: UPDATE_USER,
                    payload: res.data
                })
            }
        })
}

export const createTempUser = (user: Pick<userType, 'username' | 'avatar'>) => (dispatch: Dispatch) => {
    return axios.post<{success: boolean, user:Pick<userType, 'username' | '_id'> }>(`${process.env.REACT_APP_API}/users/validateAnonymous`, {username: user.username})
        .then((res) => {
            if (res.status === 200) {
                dispatch({
                    type: UPDATE_USER,
                    payload: {...res.data.user, avatar: user.avatar}
                })

            }
        })
}
