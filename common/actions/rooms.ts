import {Dispatch} from "redux";
import axios from "axios";
import {io, Socket} from "socket.io-client";
import {Room} from "@common/types/room/index";
import {CONNECT_SOCKET_ROOM, RESET_ROOM, START_GAME, UPDATE_BUFFER, UPDATE_ROOM} from "@storeType/index";
import {userType} from "@common/types/users/users";

export const createRoom = ( isPublic : boolean ) => (dispatch: Dispatch) => {
    return axios.post(`${process.env.REACT_APP_API}/rooms`,{
        isPrivate: !isPublic
    },{
        headers: {
            'Access-Control-Allow-Origin' : '*',
        }
    }).then((res)=> {
        if (res.status === 200) {
            return res.data;
        }
    });
}


export const connectRoom =  (
    _id: Room["_id"],
    user : userType
) => async (dispatch: Dispatch) => {
    const socket = await  io(`${process.env.REACT_APP_API}/rooms`, {
        reconnectionDelayMax: 10000,
        auth: {
            token: user.accessToken
        },
        query: {
            room_id: _id,
            user_id: user._id
        }
    });
    dispatch({
        type: CONNECT_SOCKET_ROOM,
        payload: socket
    });

    socket.emit('join',{
        roomId: _id,
        ...!user.accessToken && {
            user: {
                _id: user._id,
                avatar: user.avatar,
                username: user.username
            }
        }
    });
}


export const connectRoomState = (socket: Socket) => (dispatch: Dispatch) => {
    socket.on(`roomState`,(payload) => {
        dispatch({
            type: UPDATE_ROOM,
            payload
        })
    })
    socket.on(`data-music`,(payload) => {
        dispatch({
            type: UPDATE_BUFFER,
            payload
        })
    })
}

export const resetRoom = () => (dispatch: Dispatch) => {
    dispatch({
        type: RESET_ROOM,
    })
}

export const replay = ( socket: Socket) => {
    socket.emit('replay');
}

export const updateConfig = (config: Room["config"], socket: Socket) => {
    socket.emit('edit-config', config);
}

export const sendGuess = (data: Room["chat"][0] , socket: Socket) => {
    socket.emit('guess', data);
}


export const cleanBuffer = () => (dispatch: Dispatch) => {
    dispatch({
        type: UPDATE_BUFFER,
        payload: null
    })
}
