import {ListMusic} from "@common/types/store/reducer/listMusicsReducer";
import { AnyAction } from 'redux'
import { v4 as uuid } from 'uuid';
import {
    UPDATE_LIST_MUSIC,
    UPDATE_FILTER_MUSIC

} from "@storeType/index";

const initialState: ListMusic = {
    filters: {
        title: '',
        type: 'all',
        artists: [],
        identifier: uuid()
    },
    data: []
}

const listMusicReducer = (state = initialState, action: AnyAction) => {
    switch (action.type) {
        case UPDATE_LIST_MUSIC:
            return({
                ...state,
                data: action.payload,
            })
        case UPDATE_FILTER_MUSIC:
            return({
                ...state,
                filters: {
                    ...state.filters,
                    ...action.payload,
                    identifier: uuid()
                },
            })
        default:
            return state;
    }
}

export default listMusicReducer;
