import {createMusicForm, createMusicType} from "@common/types/store/reducer/createMusicReducer";

import { AnyAction } from 'redux'
import {
    UPDATE_ERROR_CREATE_MUSIC,
    UPDATE_FIELD_CREATE_MUSIC,
    RESET_FIELDS_CREATE_MUSIC,
    RESET_FIELD_CREATE_MUSIC
} from "@storeType/index";
import {errorDefault} from "@common/defaultValue/form";
import {MusicType} from "@common/types/musics";

const initialState: createMusicType = {
    form : {
        title: "",
        authors: [],
        type: MusicType.MUSIC,
        keywords: [],
        file: undefined
    },
    error:{
        title: errorDefault,
        authors: errorDefault,
        type: errorDefault,
        keywords: errorDefault,
        file: errorDefault,
    }
}

const createMusicReducer = (state = initialState, action: AnyAction) => {
    switch (action.type) {
        case UPDATE_FIELD_CREATE_MUSIC:
            const {field, value} = action.payload;
            return({
                ...state,
                form: {
                    ...state.form,
                    [field] : value
                }
            })
        case UPDATE_ERROR_CREATE_MUSIC:
            const {field : fieldError, value: valueError} = action.payload;
            return({
                ...state,
                error: {
                    ...state.error,
                    [fieldError] : valueError
                }
            })
        case RESET_FIELD_CREATE_MUSIC:
            const resetField = initialState.form[action.payload as keyof createMusicForm];
            return {
                ...state,
                form: {
                    ...state.form,
                    [action.payload] : resetField
                },
                error: {
                    ...state.error,
                    [action.payload]: errorDefault
                }
            };
        case RESET_FIELDS_CREATE_MUSIC:
            return initialState;
        default:
            return state;
    }
}

export default createMusicReducer;
