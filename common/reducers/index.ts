import {storeType} from "@common/types/store";
import {combineReducers} from 'redux';

import userReducer from "./userReducer";
import {applyMiddleware, createStore} from "redux";
import thunk from "redux-thunk";
import createMusicReducer from "@reducers/createMusicReducer";
import listMusicReducer from "@reducers/listMusicReducer";
import listPlaylistReducer from "@reducers/listPlaylistReducer";
import editPlaylistReducer from "@reducers/editPlaylistReducer";
import roomReducer from "@reducers/roomReducer";
import playerReducer from "@reducers/playerReducer";

const rootReducer = combineReducers<storeType>({
    user: userReducer,
    createMusic: createMusicReducer,
    listMusic: listMusicReducer,
    editPlaylist:editPlaylistReducer,
    listPlaylist: listPlaylistReducer,
    room: roomReducer,
    player: playerReducer
});

export const store = createStore(rootReducer, applyMiddleware(thunk));
