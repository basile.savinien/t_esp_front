import {ListPlaylist} from "@common/types/store/reducer/listPlaylistsReducer";
import {AnyAction} from 'redux'
import {initialSearch} from "@common/defaultValue/playlist";
import {v4 as uuid} from "uuid";
import {DELETE_PLAYLIST, SWITCH_TAB_PLAYLIST, UPDATE_FILTER_PLAYLIST, UPDATE_LIST_PLAYLIST} from "@storeType/index";
import {PlaylistTab} from "@common/types/playlists";

const initialState: ListPlaylist = {
    filters: initialSearch,
    tab: PlaylistTab.PUBLIC,
    public: {
        data: [],
        identifier: null
    },
    saved: {
        data: [],
        identifier: null
    },
    mine: {
        data: [],
        identifier: null
    },
}

const listPlaylistReducer = (state = initialState, action: AnyAction) => {
    switch (action.type) {
        case SWITCH_TAB_PLAYLIST:
            return({
                ...state,
                tab: action.payload
            })
        case UPDATE_LIST_PLAYLIST:
            return({
                ...state,
                [action?.payload?.tab] : {
                    data: action.payload?.data,
                    identifier: action.payload?.identifier
                }
            })
        case UPDATE_FILTER_PLAYLIST:
            return({
                ...state,
                filters: {
                    ...state.filters,
                    ...action.payload,
                    identifier: uuid()
                },
            })
        case DELETE_PLAYLIST:
            return({
                ...state,
                public: {
                    ...state.public,
                    data: state.public.data.filter((playlists) => playlists._id !== action.payload)
                },
                saved: {
                    ...state.saved,
                    data: state.saved.data.filter((playlists) => playlists._id !== action.payload)
                },
                mine: {
                    ...state.mine,
                    data: state.mine.data.filter((playlists) => playlists._id !== action.payload)
                },

            })
        default:
            return state;
    }
}

export default listPlaylistReducer;
