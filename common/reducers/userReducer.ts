import { AnyAction } from 'redux'
import {DELETE_PLAYLIST, FETCH_USER, LOGOUT, UPDATE_USER} from "@storeType/index";
import {UserRole, userType} from "@common/types/users";

const initialState: userType = {
    isAuth: false,
    hasFetch: false,
    role: UserRole.GUEST
}

const userReducer = (state = initialState, action: AnyAction) => {
    switch (action.type) {
        case FETCH_USER :
            return {
                ...state,
                ...action.payload,
                isAuth: true,
                hasFetch: true
            }
        case UPDATE_USER :
            return{
                ...state,
                ...action.payload
            }
        case DELETE_PLAYLIST:
            return {
                ...state,
                ...(state.playlistSaved) && {
                    playlistSaved : state.playlistSaved.filter((playlistIds) => playlistIds !== action.payload)
                }
            }
        case LOGOUT :
            return initialState;
        default:
            return state;
    }
}

export default userReducer;
