import { AnyAction } from 'redux'
import {RoomReducer} from "@common/types/store/reducer/roomReducer";
import {CONNECT_SOCKET_ROOM, UPDATE_BUFFER, UPDATE_ROOM, RESET_ROOM} from "@storeType/index";

const initialState: RoomReducer = {
    data: null,
    socket: null,
    buffer: null
}

const roomSocket = (state = initialState, action: AnyAction) => {
    switch (action.type) {
        case CONNECT_SOCKET_ROOM:
            return({
                ...state,
                socket: action.payload
            })
        case UPDATE_ROOM:
            return({
                ...state,
                data: action.payload
            })
        case UPDATE_BUFFER:
            return({
                ...state,
                buffer: action.payload
            })
        case RESET_ROOM:
            return initialState;
        default:
            return state;
    }
}

export default roomSocket;
