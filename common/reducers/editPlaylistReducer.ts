import { editPlaylistType} from "@common/types/store/reducer/editPlaylistReducer";
import { AnyAction } from 'redux'
import { v4 as uuid } from 'uuid';
import {errorDefault} from "@common/defaultValue/form";
import {
    UPDATE_FIELD_EDIT_PLAYLIST,
    ADD_MUSIC_TO_EDIT_PLAYLIST,
    REMOVE_MUSIC_TO_EDIT_PLAYLIST,
    UPDATE_FILTER_EDIT_PLAYLIST,
    UPDATE_SEARCH_EDIT_PLAYLIST,
    RESET_FIELDS_EDIT_PLAYLIST, UPDATE_FIELDS_EDIT_PLAYLIST
} from "@storeType/index";

const initialState: editPlaylistType = {
    id: null,
    form: {
        name: '',
        private: false,
        musics: []
    },
    error: {
        name: errorDefault,
        musics: errorDefault
    },
    search: {
        filters: {
            title: '',
            type: 'all',
            artists: [],
            identifier: uuid()
        },
        data: []
    }
}

const editPlaylistReducer = (state = initialState, action: AnyAction) => {
    switch (action.type) {
        case UPDATE_FIELD_EDIT_PLAYLIST:
            const {field, value} = action.payload;
            return({
                ...state,
                form: {
                    ...state.form,
                    [field] : value
                }
            })
        case UPDATE_FIELDS_EDIT_PLAYLIST:
            const data = action.payload;
            return({
                ...state,
                form: {
                    ...state.form,
                    ...data
                }
            })
        case ADD_MUSIC_TO_EDIT_PLAYLIST:
            return({
                ...state,
                form: {
                    ...state.form,
                    musics : [...state.form.musics, action.payload]
                },
            })
        case REMOVE_MUSIC_TO_EDIT_PLAYLIST:
            return({
                ...state,
                form: {
                    ...state.form,
                    musics : state.form.musics.filter((val) => val._id !== action.payload._id)
                },
            })
        case UPDATE_FILTER_EDIT_PLAYLIST:
            return({
                ...state,
                search: {
                    ...state.search,
                    filters: {
                        ...state.search.filters,
                        ...action.payload,
                        identifier: uuid()
                    }
                }
            })
        case UPDATE_SEARCH_EDIT_PLAYLIST:
            return({
                ...state,
                search : {
                    ...state.search,
                    data: action.payload
                }
            })
        case RESET_FIELDS_EDIT_PLAYLIST:
            const {id } = action;
            return ({
                ...state,
                id,
                form: initialState.form,
                error: initialState.error
            })
        default:
            return state;
    }
}

export default editPlaylistReducer;
