import { AnyAction } from 'redux'
import {Player} from "@common/types/player/index";
import {RESET_PLAYER} from "@storeType/index";

type initialStateType = (audio: AudioContext) => Player;

const initialState: initialStateType = (audio: AudioContext = (new AudioContext())) => ({
    audio,
    volume: audio.createGain(),
})

const playerReducer = (state = initialState(new AudioContext()), action: AnyAction) => {
    switch (action.type) {
        case RESET_PLAYER:
            return initialState(new AudioContext())
        default:
            return state;
    }
}

export default playerReducer;
