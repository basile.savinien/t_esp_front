#!/bin/bash

set -f

DEPLOY_SERVERS=$DEPLOY_SERVERS
ALL_SERVERS=(${DEPLOY_SERVERS//,/ })
echo "ALL_SERVERS ${ALL_SERVERS}"

for server in "${ALL_SERVERS[@]}"
do
    echo "deploying to server ${server}"
    ssh -o StrictHostKeyChecking=no ubuntu@${server} 'bash' < ./deploy/restart.sh
    echo "done"
done
